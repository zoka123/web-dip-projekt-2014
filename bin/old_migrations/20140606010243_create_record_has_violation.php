<?php

use Phpmig\Migration\Migration;

class CreateRecordHasViolation extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE record_has_violation(
            record_has_violation_id int primary key auto_increment,
            record_id int not null,
            violation_id int not null,
            reporter_id int not null,

            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null,

            FOREIGN KEY (record_id) REFERENCES record(record_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (violation_id) REFERENCES violation(violation_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (reporter_id) REFERENCES user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE record_has_violation;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
