<?php

use Phpmig\Migration\Migration;

class PoliceOfficerStationSeed extends Migration
{

    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];
        $this->db->query("DELETE FROM police_station_has_police_officer");


        $this->faker = Faker\Factory::create();
        for($i = 1; $i <= 200; $i++){
            $stationOfficer = new PoliceStationPoliceOfficer();
            $stationOfficer->police_officer_id = rand(1,50);
            $stationOfficer->police_station_id = rand(1,50);
            $stationOfficer->active_from = date('Y-m-d H:i:s',$this->faker->dateTimeThisDecade->getTimestamp());
            $stationOfficer->active_to = null;
            $stationOfficer->save();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];
        $this->db->query("DELETE FROM police_station_has_police_officer");
    }
}
