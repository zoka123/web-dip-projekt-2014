<?php

use Phpmig\Migration\Migration;

class CreateLogType extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE log_type(
            log_type_id int primary key auto_increment,
            name varchar(45) not null,

            modified_at timestamp null,
            deleted_at timestamp null,
            created_at timestamp null

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE log_type;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
