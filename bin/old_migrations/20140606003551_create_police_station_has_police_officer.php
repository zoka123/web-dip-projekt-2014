<?php

use Phpmig\Migration\Migration;

class CreatePoliceStationHasPoliceOfficer extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE police_station_has_police_officer(
            id int primary key auto_increment,
            police_officer_id int not null,
            police_station_id int not null,

            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null,

            FOREIGN KEY (police_officer_id) REFERENCES police_officer(police_officer_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (police_station_id) REFERENCES police_station(police_station_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE police_station_has_police_officer;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
