<?php

require_once dirname(__DIR__).DIRECTORY_SEPARATOR."vendor/autoload.php";
require_once dirname(__DIR__).DIRECTORY_SEPARATOR."functions.php";
error_reporting(E_ALL);


# phpmig.php

use \Phpmig\Adapter,
    \Pimple;


$container = new Pimple();

Config::init();

$container['db'] = $container->share(function() {
        $dbh = DB::getPDO();
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $dbh->query("SET NAMES utf8");
        return $dbh;
    });

$container['phpmig.adapter'] = $container->share(function() use ($container) {
        return new Adapter\PDO\Sql($container['db'], 'migrations');
    });

$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

return $container;
