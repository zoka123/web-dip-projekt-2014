<?php

use Phpmig\Migration\Migration;

class CreateUser extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();
        $queries[] = <<<SQL
        CREATE TABLE user(
            user_id int primary key auto_increment,
            user_role_id int not null default 3,
            oib varchar(11) not null,
            first_name varchar(45) not null,
            last_name varchar(45) not null,
            email varchar(45) not null,
            password varchar(255) not null,
            birthdate datetime not null,
            address text null,
            attempts int default 0,
            description text null,
            telephone varchar(15) null,
            last_login_at timestamp null,

            enabled boolean not null default 0,
            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null,

            FOREIGN KEY fk_user_role (user_role_id) REFERENCES user_role(user_role_id)  ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE user;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
