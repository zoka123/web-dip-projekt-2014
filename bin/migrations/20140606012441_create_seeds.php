<?php

use Phpmig\Migration\Migration;

class CreateSeeds extends Migration
{

    private $db;
    /**
     * @var Faker\Generator $faker
     */
    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        try {
            $container = $this->getContainer();
            $this->db = $container['db'];


            $this->faker = Faker\Factory::create();

            echo "Seeding user roles";
            $this->seedUserRoles();

            echo "Seeding users";
            $this->seedUsers();

            echo "Seeding Counties";
            $this->seedCounties();

            echo "Seeding Cities";
            $this->seedCities();


        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {
    }


    public function seedCounties()
    {
        $sql = include("data/countiesData.php");
        $this->db->query($sql);
    }

    public function seedCities()
    {
        $sql = include("data/citiesData.php");
        $this->db->query($sql);
    }

    public function seedUserRoles()
    {
        $userRole = new UserRole();
        $userRole->name = "Administrator";
        $userRole->save();

        $userRole2 = new UserRole();
        $userRole2->name = "Moderator";
        $userRole2->save();

        $userRole3 = new UserRole();
        $userRole3->name = "Registered";
        $userRole3->save();
    }

    public function seedUsers()
    {
        $this->db->query("DELETE FROM user");

        $user = new User();
        $user->user_role_id = 1;
        $user->first_name = "Administrator";
        $user->last_name = "Admin";
        $user->email = "admin@foi.hr";
        $user->oib = "12345678912";
        $user->password = "123456";
        $user->birthdate = date('Y-m-d H:i:s',strtotime("25.9.1992."));
        $user->address = $this->faker->streetAddress;
        $user->description = $this->faker->sentence;
        $user->enabled = true;
        $user->save();

        $user = new User();
        $user->user_role_id = 2;
        $user->first_name = "Moderator";
        $user->last_name = "Mod";
        $user->email = "moderator@foi.hr";
        $user->oib = "12345678923";
        $user->password = "123456";
        $user->birthdate = date('Y-m-d H:i:s',strtotime("25.9.1992."));
        $user->address = $this->faker->streetAddress;
        $user->description = $this->faker->sentence;
        $user->enabled = true;
        $user->save();

        $user = new User();
        $user->user_role_id = 3;
        $user->first_name = "Register";
        $user->last_name = "Reg";
        $user->email = "user@foi.hr";
        $user->oib = "12345678934";
        $user->password = "123456";
        $user->birthdate = date('Y-m-d H:i:s',strtotime("25.9.1992."));
        $user->address = $this->faker->streetAddress;
        $user->description = $this->faker->sentence;
        $user->enabled = true;
        $user->save();


        for ($i = 1; $i <= 100; $i++) {
            $user = new User();
            $user->user_role_id = rand(1, 3);
            $user->first_name = $this->faker->firstName;
            $user->last_name = $this->faker->lastName;
            $user->email = $this->faker->email;
            $user->oib = $i;
            $user->password = "123456";
            $user->birthdate = date('Y-m-d H:i:s',$this->faker->dateTimeThisCentury->getTimestamp());
            $user->address = $this->faker->streetAddress;
            $user->description = $this->faker->sentence;
            $user->last_login_at = date('Y-m-d H:i:s',$this->faker->dateTimeThisMonth->getTimestamp());
            $user->enabled = true;
            $user->save();
        }
    }
}
