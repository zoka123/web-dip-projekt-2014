<?php

use Phpmig\Migration\Migration;

class CreatePoliceOfficer extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE police_officer(
            police_officer_id int primary key auto_increment,
            user_id int not null,
            police_station_id int null,
            licence_number int not null,
            licence_expiration_date datetime not null,

            enabled boolean not null default 1,
            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null,

            FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (police_station_id) REFERENCES police_station(police_station_id) ON DELETE RESTRICT ON UPDATE CASCADE
            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE police_officer;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
