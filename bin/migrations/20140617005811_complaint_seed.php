<?php

use Phpmig\Migration\Migration;

class ComplaintSeed extends Migration
{
    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];

        $this->db->query("DELETE FROM complaint");

        $this->faker = Faker\Factory::create();
        for($i = 1; $i <=100; $i++){
            $complaint = new Complaint();
            $complaint->complaint_status_id = rand(1,3);
            $complaint->violation_id = rand(1,100);
            $complaint->text = $this->faker->sentence();
            $complaint->save();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
