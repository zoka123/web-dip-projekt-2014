<?php

use Phpmig\Migration\Migration;

class StationsSeeds extends Migration
{

    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];

        $this->db->query("DELETE FROM police_station");
        $this->faker = Faker\Factory::create();

        for($i = 1; $i <= 50; $i++){
            $station = new PoliceStation();
            $station->county_id = rand(1,21);
            $station->name = $this->faker->word;
            $station->address = $this->faker->streetAddress;
            $station->telephone = $this->faker->phoneNumber;
            $station->enabled = true;
            $station->save();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
