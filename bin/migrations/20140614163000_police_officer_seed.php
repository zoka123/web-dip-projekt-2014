<?php

use Phpmig\Migration\Migration;

class PoliceOfficerSeed extends Migration
{

    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];

        $this->faker = Faker\Factory::create();
        $this->db->query("DELETE FROM police_officer");
        for($i = 1; $i <= 50; $i++){
            $officer = new PoliceOfficer();
            $officer->user_id = 51-$i;
            $officer->licence_number = rand(1,1000);
            $officer->police_station_id = rand(1,50);
            $officer->licence_expiration_date = date('Y-m-d H:i:s',$this->faker->dateTimeThisDecade->getTimestamp());
            $officer->save();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
