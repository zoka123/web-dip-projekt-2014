<?php
/**
 * Author: Zoran Antolovic
 * Date: 14.06.14.
 * Time: 12:18
 */

return "INSERT INTO `county` (`county_id`, `name`, `enabled`) VALUES
(1, 'Zagrebačka', 1),
(2, 'Krapinsko-zagorska', 1),
(3, 'Sisačko-moslavačka', 1),
(4, 'Karlovačka', 1),
(5, 'Varaždinska', 1),
(6, 'Koprivničko-križevačka', 1),
(7, 'Bjelovarsko-bilogorska', 1),
(8, 'Primorsko-goranska', 1),
(9, 'Ličko-senjska', 1),
(10, 'Virovitičko-podravska', 1),
(11, 'Požeško-slavonska', 1),
(12, 'Brodsko-posavska', 1),
(13, 'Zadarska', 1),
(14, 'Osječko-baranjska', 1),
(15, 'Šibensko-kninska', 1),
(16, 'Vukovarsko-srijemska', 1),
(17, 'Splitsko-dalmatinska', 1),
(18, 'Istarska', 1),
(19, 'Dubrovačko-neretvanska', 1),
(20, 'Međimurska', 1),
(21, 'Grad Zagreb', 1);";