<?php

use Phpmig\Migration\Migration;

class CreateViolation extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE violation(
            violation_id int primary key auto_increment,
            violation_category_id int not null,
            reporter_id int not null,
            user_id int not null,
            violation_ts timestamp not null,
            city_id int not null,
            description text not null,

            deleted_at timestamp null,
            modified_at timestamp null,
            created_at timestamp null,

            FOREIGN KEY (violation_category_id) REFERENCES violation_category(violation_category_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (city_id) REFERENCES city(city_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (reporter_id) REFERENCES police_officer(police_officer_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE violation;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
