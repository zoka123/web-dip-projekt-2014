<?php

use Phpmig\Migration\Migration;

class CreateComplaint extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE complaint(
            complaint_id int primary key auto_increment,
            violation_id int not null,
            complaint_status_id int not null,
            text text not null,

            deleted_at timestamp null,
            modified_at timestamp null,
            created_at timestamp null,

            FOREIGN KEY (violation_id) REFERENCES violation(violation_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE complaint;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
