<?php

use Phpmig\Migration\Migration;

class ViolationCategorySeed extends Migration
{

    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];

        $this->faker = Faker\Factory::create();

        $this->db->query("DELETE FROM violation_category");

        $category = new ViolationCategory();
        $category->name = "Krađa";
        $category->period_of_limitation = ViolationCategory::PERIOD_TWO_YEARS;
        $category->enabled = 1;
        $category->save();

        $category = new ViolationCategory();
        $category->name = "Ubojstvo";
        $category->period_of_limitation = null;
        $category->enabled = 1;
        $category->save();

        $category = new ViolationCategory();
        $category->name = "Prometni prekršaj";
        $category->period_of_limitation = ViolationCategory::PERIOD_SIX_MONTH;
        $category->enabled = 1;
        $category->save();

        $category = new ViolationCategory();
        $category->name = "Pogrešno parkiranje";
        $category->period_of_limitation = ViolationCategory::PERIOD_ONE_WEEK;
        $category->enabled = 1;
        $category->save();
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
