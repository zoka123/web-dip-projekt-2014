<?php

use Phpmig\Migration\Migration;

class CreateRecord extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE record(
            record_id int primary key auto_increment,
            offender_id int not null,
            creator_id int not null,

            enabled boolean not null default 1,
            deleted_at timestamp null,
            modified_at timestamp null,
            created_at timestamp null,

            FOREIGN KEY (offender_id) REFERENCES user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (creator_id) REFERENCES user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE record;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
