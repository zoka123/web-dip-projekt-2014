<?php

use Phpmig\Migration\Migration;

class LogSeed extends Migration
{
    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];

        $this->db->query("DELETE FROM log");

        $this->faker = Faker\Factory::create();
        for($i = 1; $i <=100; $i++){
            $log = new Log();
            $log->log_type = rand(0,3);
            $log->query = $this->faker->word;
            $log->additional = $this->faker->word;
            $log->created_at = $this->faker->dateTimeThisMonth->getTimestamp();
            $log->save();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
