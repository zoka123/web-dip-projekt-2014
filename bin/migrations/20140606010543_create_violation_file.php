<?php

use Phpmig\Migration\Migration;

class CreateViolationFile extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE violation_file(
            violation_file_id int primary key auto_increment,
            violation_id int not null,
            file_id int not null,

            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null,

            FOREIGN KEY (file_id) REFERENCES file(file_id) ON DELETE RESTRICT ON UPDATE CASCADE,
            FOREIGN KEY (violation_id) REFERENCES violation(violation_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE violation_file;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
