<?php

use Phpmig\Migration\Migration;

class ViolationSeed extends Migration
{
    private $faker;

    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $this->db = $container['db'];

        $this->db->query("DELETE FROM violation");

        $this->faker = Faker\Factory::create();
        for($i = 1; $i <=100; $i++){
            $violation = new Violation();
            $violation->violation_category_id = rand(1,4);
            $violation->violation_ts = datetime($this->faker->dateTimeThisMonth->getTimestamp());
            $violation->reporter_id = rand(1,50);
            $violation->user_id = rand(51,100);
            $violation->city_id = rand(1,800);
            $violation->description = $this->faker->sentence();
            $violation->save();
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
