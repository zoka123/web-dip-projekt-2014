<?php

use Phpmig\Migration\Migration;

class CreateViolationCategory extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE violation_category(
            violation_category_id int primary key auto_increment,
            name varchar(45) not null,
            period_of_limitation int null,

            enabled boolean not null default 1,
            deleted_at timestamp null,
            modified_at timestamp null,
            created_at timestamp null
            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE violation_category;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
