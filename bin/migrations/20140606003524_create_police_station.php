<?php

use Phpmig\Migration\Migration;

class CreatePoliceStation extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();

        $queries[] = <<<SQL
        CREATE TABLE police_station(
            police_station_id int primary key auto_increment,
            county_id int not null,
            name varchar(45) not null,
            address text null,
            telephone varchar(15) null,

            enabled boolean not null default 1,
            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null,

            FOREIGN KEY (county_id) REFERENCES county(county_id) ON DELETE RESTRICT ON UPDATE CASCADE

            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE police_station;
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

    }
}
