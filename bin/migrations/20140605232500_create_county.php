<?php

use Phpmig\Migration\Migration;

class CreateCounty extends Migration
{

    /**
     * Do the migration
     */
    public function up()
    {
        $queries = array();
        $queries[] = <<<SQL

        CREATE TABLE county(
            county_id int primary key auto_increment,
            name varchar(45) not null,

            enabled boolean not null default 1,
            modified_at timestamp null,
            created_at timestamp null,
            deleted_at timestamp null
            );
SQL;

        try {
            $container = $this->getContainer();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }


    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $queries = array();

        $queries[] = <<<SQL
        DROP TABLE county;
SQL;

        try {
            $container = $this->getContainer();
            $container['db']->beginTransaction();

            foreach ($queries as $sql) {
                $container['db']->query($sql);
            }
            $container['db']->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $container['db']->rollback();
            exit;
        }

    }
}