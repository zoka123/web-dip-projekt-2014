<?php

require_once("vendor/autoload.php");

require_once("functions.php");



require_once("application/routes.php");

Config::init();
View::init();

$app = new Application();
$app->run();


