<?php 
 return array (
  'app_mode' => 'production',
  'base_url' => 'http://localhost/webdip/projekt/',
  'template_path' => '/application/views',
  'upload_base' => 'uploads/',
  'host' => 'localhost',
  'username' => 'root',
  'password' => '',
  'database' => 'webdip_projekt',
);