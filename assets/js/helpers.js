var loader = $("#logo-loader");

var checkLock = 0;

function scrollBoxLoading(elem) {
    elem.html('<ul class="no-bullet"><li><a><i class="fa fa-spin fa-spinner"></i> <b>Učitavam</b></a></li></ul>');
}

function openModal() {
    $("body").addClass("modal-open loading");
    $(".modal").fadeIn(function () {
        $("body").removeClass("loading");
    });
}

function closeModal() {
    $("body").removeClass("modal-open");
    $(".modal").fadeOut();
}


$("body").on("click", '*[data-open="modal"]', function (e) {
    e.stopPropagation();
    openModal();
})

$("body").on("click", '*[data-close="modal"]', function (e) {
    e.stopPropagation();
    closeModal();
})

$("body").on("click", '*[data-ajax-modal="true"]', function (e) {
    e.preventDefault();
    e.stopPropagation();
    url = $(this).attr("data-ajax-url");
    $.ajax({
        url: url,
        dataType: "HTML",
        type: "GET"
    }).done(function (data) {
            $(".modal .modal-content").html(data);
            openModal();
            initControls();
        });
})

$(document).on("click", "body.modal-open #black-overlay", function (e) {
    e.stopPropagation();
    closeModal();
})

function prepareFiles(list) {
    var files = "";
    var delimiter = "";
    list.children("li.new-file").each( function () {
        files = files + delimiter + $(this).attr("data-file");
        delimiter = "###FILE###";
    });

    $("#new-files").val(files);
}

$(document).on("submit", "form.ajax-form", function (e) {
    e.preventDefault();
    $("body").addClass("loading");



    if ($('#list-files').length) {
        prepareFiles($('#list-files'));
    }

    var form = $(this);
    form.find(".message").remove();

    var url = $(this).attr("action");
    var method = $(this).attr("method");
    var data = form.serializeArray();
    console.log(data);

    $.ajax({
        url: url,
        dataType: "JSON",
        type: method,
        data: data
    }).done(function (data) {
            console.log(data);
            if (data.status == 0) {
                for (var key in data.msg) {
                    if (data.msg.hasOwnProperty(key)) {

                        if (key == "general") {
                            var errorMsg = data.msg[key];
                            var error = $('<div/>', {text: errorMsg }).addClass("error message");
                            form.prepend(error);

                        } else {
                            var label = $("#" + key).siblings("label").text();

                            var errorMsg = data.msg[key][0];
                            var errorMsg = errorMsg.replace("#", label);

                            var error = $('<div/>', {text: errorMsg }).addClass("error message");
                            error.insertAfter($("#" + key));
                        }
                    }
                }

            } else if (data.status = 1) {
                var submit = form.find('input[type="submit"]');
                submit.prop('disabled', true);

                if (data.action != undefined) {
                    eval(data.action);
                }

                if (data.message != undefined) {
                    var message = data.message;
                    var message = $('<div/>', {text: message }).addClass("success message");
                    form.prepend(message);
                }
            }
            $("body").removeClass("loading");
        });

});


$(document).on("click", '#external-crud-form button[type="submit"]', function (e) {
    window.setTimeout(function () {
        $('.pagination li a.current').click()
    }, 200);
});

$(document).on("click", ".pagination li a", function (e) {
    e.preventDefault();
    $("body").addClass("loading");
    var parent = $("#table-wrapper");
    var url = $(this).attr("href");

    $(parent).load(url + " #table-wrapper", function () {
        $("body").removeClass("loading");
    });
});

$("#paginationPerPage").on("change", function (e) {
    e.preventDefault();
    $("body").addClass("loading");

    var parent = $("#table-wrapper");
    var url = $("a.current").attr("href");
    var val = $(this).val();
    url = url.replace(/&perPage=\d+/g, '');
    url = url + "&perPage=" + val;

    $(parent).load(url + " #table-wrapper", function () {
        $("body").removeClass("loading");
        $("#paginationPerPage").val(val);
    });
});

$("#filter_select").on("change", function (e) {
    e.preventDefault();
    $("body").addClass("loading");

    var parent = $("#table-wrapper");
    var url = $("a.current").attr("href");
    var val = $(this).val();
    url = url.replace(/&filter_option=\w+/g, '');
    url = url + "&filter_option=" + val;

    $(parent).load(url + " #table-wrapper", function () {
        $("body").removeClass("loading");
        $("#filter_select").val(val);
    });
});

$("#timeFilterTrigger").on("click", function (e) {
    e.preventDefault();

    var start_filter = $("#datetime_start").val();
    var end_filter = $("#datetime_end").val();

    if (start_filter == "undefined" || end_filter == "undefined" || start_filter == "" || end_filter == "") {
        alert("Krivi datumi!");

        return;
    }

    try {
        var parent = $("#table-wrapper");
        var url = $("a.current").attr("href");
        url = url.replace(/&date_start=\d+.\d+.\d+/g, '');
        url = url.replace(/&date_end=\d+.\d+.\d+/g, '');
        url = url + "&date_start=" + start_filter;
        url = url + "&date_end=" + end_filter;
    } catch (e) {
        console.log(e);
        return;
    }
    $("body").addClass("loading");
    $(parent).load(url + " #table-wrapper", function () {
        $("body").removeClass("loading");
    });
});

$("#crud-query").on("keyup", function (e) {
    $("body").addClass("loading");

    var query = encodeURIComponent($(this).val());

    var parent = $("#table-wrapper");
    var url = $("a.current").attr("href");
    url = url.replace(/&query=(\w|\s)+/g, '');
    url = url + "&query=" + query;

    $(parent).load(url + " #table-wrapper", function () {
        $("body").removeClass("loading");
    });
});

$(document).on("click", ".table-crud th a.sort-link", function (e) {
    e.preventDefault();
    $("body").addClass("loading");
    var parent = $("#table-wrapper");
    var url = $("a.current").attr("href");
    var sort = $(this).attr("data-sort-key");
    var direction = $(this).attr("data-sort-direction");
    url = url.replace(/&sort=\w+/g, '');
    url = url.replace(/&direction=\w+/g, '');
    url = url + "&sort=" + sort;
    url = url + "&direction=" + direction;

    $(parent).load(url + " #table-wrapper", function () {
        $("body").removeClass("loading");
    });

});

function checkFree(id) {
    var elem = $("#" + id);
    val = elem.val();
    name = elem.attr("name");

    var submit = elem.closest("form").find('input[type="submit"]');
    submit.prop('disabled', true);
    elem.closest("form").find('.message.error').remove();

    $.get(base_url + "api/check/" + name + "?" + name + "=" + val, function (data) {
        if (data != 1) {
            var label = elem.siblings("label").text();

            var errorMsg = "Vrijednost polja # je zauzeta";
            var errorMsg = errorMsg.replace("#", label);

            var error = $('<div/>', {text: errorMsg }).addClass("form-check-message error message");
            error.insertAfter($("#" + name));
            elem.focus();
        } else {
            submit.prop('disabled', false);
        }
    });
}

$(document).on("blur", '.check-free', function () {
    checkFree($(this).attr("id"));
})


$(document).on("click", ".external-crud-form-trigger", function (e) {
    e.preventDefault();

    var form = $("#external-crud-form");

    if (form.is(":visible")) {
        form.slideToggle();
    }

    var url = $(this).attr("href");

    $.get(url, function (data) {
        form.html("");
        form.append(data).slideToggle();
        ;
        initControls();
    });
});

$(document).on("click", ".external-crud-form-close", function (e) {
    e.preventDefault();
    var form = $("#external-crud-form");
    form.slideUp(function () {
        form.html("");
    });
});

function externalCrudFormDisable() {
    $('#external-crud-form *[type="submit"]').prop('disabled', true).remove();
}


$(document).on("click", ".ajax-delete", function (e) {
    e.preventDefault();

    if (!confirm("Jeste li sigurni")) {
        return;
    }

    var url = $(this).attr("href");

    $.get(url, function (data) {
        if (data == 1) {
            window.location.reload();
        } else {
            try {
                if (data.msg) {
                    alert(data.msg);
                }
            } catch (e) {

            }
        }
    });
});


$(document).on("click", "#forgotten-password", function (e) {
    e.preventDefault();
    var form = $(this).closest("form");
    email = form.find('#email').val();

    if (email == "" || email == undefined) {
        alert("Unesite email");
    }

    var url = $(this).attr("href") + "?email=" + email;

    $.get(url, function (data) {
        if (data == 1) {
            alert("Lozinka je poslana na Vaš email");
        }
    });
});

$(document).on("click", "#file-upload-trigger", function () {
    var url = $(this).attr("data-upload-url");
    w = 500;
    h = 200;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    window.open(url, null,
        'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left + "height=200,width=500,status=no,toolbar=no,menubar=no,location=no");
})