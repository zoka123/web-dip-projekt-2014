<?php

class Paginator
{

    public $columns = array();
    public $count = null;

    public $timeFilterColumn = "created_at";

    const DEFAULT_PAGINATION_SORT = 1;
    const DEFAULT_PAGINATION_SORT_DIRECTION = "ASC";
    const DEFAULT_PAGINATION_PER_PAGE = 5;
    const DEFAULT_PAGINATION_PAGE = 1;

    public $SQLParams = array();
    public $pages;
    public $activeSort;
    public $activeDirection;


    public function setColumns($columns)
    {
        foreach ($columns as $k => $column) {
            $this->columns[$k] = array("label" => $column, "sort" => "ASC", "currentSort" => "ASC");
        }
    }

    public function setCount($count)
    {
        $this->count = $count;
    }


    public function prepare()
    {
        $this->getTablePagination();

        if (in_array($this->activeSort, array_keys($this->columns))) {
            if ($this->activeDirection == "ASC") {
                $this->columns[$this->activeSort]["sort"] = "DESC";
                $this->columns[$this->activeSort]["currentSort"] = "ASC";
            } else {
                $this->columns[$this->activeSort]["sort"] = "ASC";
                $this->columns[$this->activeSort]["currentSort"] = "DESC";
            }
        }
    }

    public function getSQLParams($where = array())
    {
        $params = $this->SQLParams;

        if (Input::get("query")) {
            $query = urldecode(Input::get("query"));

            $like = " ( ";
            $separator = "";
            foreach ($this->columns as $k => $v) {
                $like .= $separator . $k . " LIKE \"%" . $query . "%\" ";
                $separator = " OR ";
            }
            $like .= " )";
            $where[] = $like;
            unset($params["limit"]);
        }

        if (Input::get("date_start") && Input::get("date_end")) {
            $start = datetime(Input::get("date_start"));
            $end = datetime(Input::get("date_end"));
            $timeFilterColumn = $this->timeFilterColumn;
            $filter = " {$timeFilterColumn} >= \"" . $start . "\" AND {$timeFilterColumn} <= \"" . $end ."\"";

            $where[] .=  $filter;
        }

        $params["where"] = implode(" AND ", $where);


        return $params;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function getActiveSort()
    {
        return $this->activeSort;
    }

    public function getActiveDirection()
    {
        return $this->activeDirection;
    }


    public function getTablePagination()
    {
        $input = Input::all();
        $params = array("params" => array(), "links" => array());

        if (!isset($input["sort"])) {
            $input["sort"] = static::DEFAULT_PAGINATION_SORT;
        }
        if (!isset($input["direction"]) || !($input["direction"] == "ASC" || $input["direction"] == "DESC")) {
            $input["direction"] = static::DEFAULT_PAGINATION_SORT_DIRECTION;
        }
        if (!isset($input["perPage"])) {
            $input["perPage"] = static::DEFAULT_PAGINATION_PER_PAGE;
        }
        if (!isset($input["page"])) {
            $input["page"] = static::DEFAULT_PAGINATION_PAGE;
        }

        $params["order"] = $input["sort"] . " " . $input["direction"];
        $params["limit"] = ((int)$input["perPage"] * (int)$input["page"]) - (int)$input["perPage"] . ", " . (int)$input["perPage"];

        $totalPages = (int)ceil((int)$this->count / (int)$input["perPage"]);
        $links = array();
        $links["start"] = 1;

        $paginationSpan = 5;
        $paginationOffset = (int)($paginationSpan / 2);

        if ($paginationSpan >= $totalPages) {
            $start = 1;
            $end = $totalPages;
        } else {

            if ($input["page"] > $paginationOffset && $input["page"] < $totalPages - $paginationOffset) {
                $start = $input["page"] - $paginationOffset;
                $end = $input["page"] + $paginationOffset;
            } else {
                if ($input["page"] <= $paginationOffset) {
                    $start = 1;
                    $end = $input["page"] + $paginationOffset;
                } elseif ($input["page"] >= $totalPages - $paginationOffset) {
                    $start = $input["page"] - $paginationOffset;
                    $end = $totalPages;
                }
            }
        }

        for ($i = $start; $i <= $end; $i++) {
            if ($i == $input["page"]) {
                $links["current"] = $i;
            } else {
                $links[] = $i;
            }
        }
        $links["end"] = $totalPages;

        $this->SQLParams = $params;
        $this->pages = $links;
        $this->activeSort = $input["sort"];
        $this->activeDirection = $input["direction"];
    }

    public function getPaginationLinks($params = array())
    {
        $pages = $this->pages;

        $links = array();
        if (isset($params["page"])) {
            unset($params["page"]);
        }


        foreach ($pages as $k => $page) {
            if ($k === "start") {
                $label = "Početak";
            } elseif ($k === "end") {
                $label = "Kraj";
            } else {
                $label = $page;
            }

            $paramsString = "";
            foreach ($params as $key => $param) {
                $paramsString .= "&" . $key . "=" . $param;
            }

            $class = "";
            if ($k === "current") {
                $class = "current";
            }

            $links[] = array("label" => $label, "class" => $class, "url" => "?page=" . $page . $paramsString);
        }

        return $links;
    }

} 