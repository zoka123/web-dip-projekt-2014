<?php

class HTMLHelper
{

    public static $defaultPaginationParams = array(
        "sort" => 1,
        "direction" => "ASC",
        "perPage" => 10,
        "page" => 1
    );

    public static function getTablePagination($count)
    {
        $input = Input::all();
        $params = array("params" => array(), "links" => array());

        if (!isset($input["sort"])) {
            $input["sort"] = static::$defaultPaginationParams["sort"];
        }
        if (!isset($input["direction"]) || !($input["direction"] == "ASC" || $input["direction"] == "DESC")) {
            $input["direction"] = static::$defaultPaginationParams["direction"];
        }
        if (!isset($input["perPage"])) {
            $input["perPage"] = static::$defaultPaginationParams["perPage"];
        }
        if (!isset($input["page"])) {
            $input["page"] = static::$defaultPaginationParams["page"];
        }

        $params["order"] = $input["sort"] . " " . $input["direction"];
        $params["limit"] = ((int)$input["perPage"] * (int)$input["page"]) - (int)$input["perPage"] . ", " . (int)$input["perPage"];


        $totalPages = (int)ceil((int)$count / (int)$input["perPage"]);
        $links = array();
        $links["start"] = 1;

        $paginationSpan = 5;
        $paginationOffset = (int)($paginationSpan / 2);


        if ($paginationSpan >= $totalPages ) {
            $start = 1;
            $end = $totalPages;
        } else {

            if ($input["page"] > $paginationOffset && $input["page"] < $totalPages - $paginationOffset) {
                $start = $input["page"] - $paginationOffset;
                $end = $input["page"] + $paginationOffset;
            } else {
                if ($input["page"] <= $paginationOffset) {
                    $start = 1;
                    $end = $input["page"] + $paginationOffset;
                } elseif ($input["page"] >= $totalPages - $paginationOffset) {
                    $start = $input["page"] - $paginationOffset;
                    $end = $totalPages;
                }
            }
        }

        for ($i = $start; $i <= $end; $i++) {
            if ($i == $input["page"]) {
                $links["current"] = $i;
            } else {
                $links[] = $i;
            }
        }
        $links["end"] = $totalPages;

        return array("pages" => $links, "params" => $params, "sort" => $input["sort"], "direction" => $input["direction"]);


    }

    public static function getPaginationLinks($pages, $params = array())
    {
        $links = array();
        if (isset($params["page"])) {
            unset($params["page"]);
        }


        foreach ($pages as $k => $page) {
            if ($k === "start") {
                $label = "Početak";
            } elseif ($k === "end") {
                $label = "Kraj";
            } else {
                $label = $page;
            }

            $paramsString = "";
            foreach ($params as $key => $param) {
                $paramsString .= "&" . $key . "=" . $param;
            }

            $class = "";
            if ($k === "current") {
                $class = "current";
            }

            $links[] = array("label" => $label, "class" => $class, "url" => "?page=" . $page . $paramsString);
        }


        return $links;

    }


} 