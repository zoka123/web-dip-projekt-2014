<?php

class Menu
{

    private $items = array();
    private $active = null;
    private $template = null;

    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * @param null $active
     */
    public function setActiveItem($activeItem)
    {
        $this->active = $activeItem;
    }

    /**
     * @return null
     */
    public function getActiveItem()
    {
        return $this->activeItem;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        foreach($items as $key => $item){
            if(strpos($key,"@") !== false){
                $key = Router::route($key);
            }
            $this->items[$key] = $item;
        }
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param null $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return null
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function render()
    {
        return View::render($this->template, array("items" => $this->items, "active" => $this->active));
    }

    public function __toString()
    {
        try {
            return (string)$this->render();
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        return "";
    }

    public static function getMenuByUserRole($userRoleId){


        switch($userRoleId){
            case User::REGISTERED:
                return "registered-menu";
                break;
            case User::MODERATOR:
                return "moderator-menu";
                break;
            case User::ADMIN:
                return "admin-menu";
                break;
        }
    }


} 