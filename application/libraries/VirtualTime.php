<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 16.06.14.
 * Time: 01:18
 */

class VirtualTime {

    private static $url = "http://arka.foi.hr/PzaWeb/PzaWeb2004/config/pomak.xml";
    public static $offset = null;

    public static function get(){
        $xml = file_get_contents(self::$url);

        $data = simplexml_load_string($xml);
        $pomak = (int) $data->vrijeme->pomak["brojSati"][0];

        $config = '<?php $tempTimeOffset = ' . $pomak . ";";

        $fh = fopen(Config::base() . "/cache/time_temp.php", "w");
        if(fwrite($fh,$config)){
            echo 1;
        }
        fclose($fh);
    }

    public static function activate(){
        require(Config::base() . "/cache/time_temp.php");

        $fh = fopen(Config::base() . "/cache/time.php", "w");
        $config = $config = '<?php $tempTimeOffset = ' . $tempTimeOffset . ";";
        if(fwrite($fh,$config)){
            echo 1;
        }
        fclose($fh);
    }

    public static function init(){
        if(static::$offset == null){
            require(Config::base() . "/cache/time.php");
            static::$offset = $tempTimeOffset;
        }
    }

    public static function vtime(){
        self::init();
        return time() + self::$offset*3600;
    }

} 