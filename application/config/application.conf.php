<?php

$conf = array(
    "app_mode" => "production",
    "base_url" => "http://localhost/webdip/projekt/",
    "template_path" => "/application/views",
    "upload_base" => "uploads/",
);


$arkaConf = array(
    "app_mode" => "production",
    "base_url" => "http://arka.foi.hr/WebDiP/2013_projekti/WebDiP2013_002/",
    "template_path" => "/application/views",
    "upload_base" => "uploads/",
);


$dev = array(
    '127.0.0.1',
    '::1'
);

if(!isset($_SERVER['REMOTE_ADDR']) || in_array($_SERVER['REMOTE_ADDR'], $dev)){
    return $conf;
} else {
    return $arkaConf;
}
