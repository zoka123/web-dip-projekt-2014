<?php

/* Public routes*/
Router::add('/', "PublicController@index");
Router::add('login', "PublicController@login");
Router::add('register', "PublicController@register");

Router::add('api/counties', "CountyController@allCounties");
Router::add('api/stationsByCounty/(\d+)', "PublicController@stationsByCounty");
Router::add('api/officersByStation/(\d+)', "PublicController@officersByStation");
Router::add('api/officer/(\d+)', "PublicController@officerDetails");
Router::add('api/check/(\w+)', "PublicController@check");
Router::add('api/violations/ending-today', "AdminController@ending");
Router::add('api/violations/ending-future', "AdminController@endingFuture");
Router::add('api/complaint-details/(\d+)', "ComplaintController@details");
Router::add('api/violation-details/(\d+)', "ViolationController@details");

Router::add('admin', "AdminController@index");
Router::add('moderator', "ModeratorController@index");
Router::add('registered', "RegisteredController@index");
Router::add('user/(\d+)', "RegisteredController@index");

Router::add('countyStatsPDF', "ModeratorController@countyStatsPDF");

Router::add('reset', "PublicController@reset");
Router::add('users', "UserController@all");
Router::add('cities', "AdminController@cities");
Router::add('counties', "CountyController@all");
Router::add('stations', "StationController@all");
Router::add('officers', "OfficerController@all");
Router::add('violation-categories', "ViolationController@allCategories");
Router::add('violations', "ViolationController@all");
//Router::add('complaints', "ComplaintController@all");
Router::add('violations-old', "ViolationController@allOld");
Router::add('violations-active', "ViolationController@allActive");
Router::add('logs', "LogController@all");

Router::add('user/edit/(\d+)', "UserController@edit");
Router::add('user/add', "UserController@add");
Router::add('user/delete/(\d+)', "UserController@delete");
Router::add('user/activate/(\d+)/(\w+)', "PublicController@activate");

Router::add('city/edit/(\d+)', "AdminController@cityEdit");
Router::add('city/add', "AdminController@cityAdd");
Router::add('city/delete/(\d+)', "AdminController@cityDelete");

Router::add('county/edit/(\d+)', "CountyController@edit");
Router::add('county/add', "CountyController@add");
Router::add('county/delete/(\d+)', "CountyController@delete");

Router::add('log/edit/(\d+)', "LogController@edit");
Router::add('log/add', "LogController@add");
Router::add('log/delete/(\d+)', "LogController@delete");

Router::add('violation-category/edit/(\d+)', "ViolationController@editCategory");
Router::add('violation-category/add', "ViolationController@addCategory");
Router::add('violation-category/delete/(\d+)', "ViolationController@deleteCategory");

Router::add('violation/edit/(\d+)', "ViolationController@edit");
Router::add('violation/add', "ViolationController@add");
Router::add('violation/delete/(\d+)', "ViolationController@delete");

Router::add('complaint/add/(\d+)', "ComplaintController@add");
Router::add('complaint/accept/(\d+)', "ComplaintController@accept");
Router::add('complaint/reject/(\d+)', "ComplaintController@reject");

Router::add('station/edit/(\d+)', "StationController@edit");
Router::add('station/add', "StationController@add");
Router::add('station/delete/(\d+)', "StationController@delete");
Router::add('station/add-officers/(\d+)', "StationController@addOfficers");


Router::add('upload/violationFile', "FileController@violationFile");


Router::add('time/get', function () {
        VirtualTime::get();
        exit;
    });

Router::add('time/activate', function () {
        VirtualTime::activate();
        exit;
    });

Router::add('logout', "PublicController@logout");



/********************************************************/

