<?php


class Email
{

    private static $body = null;

    public static function compose($body)
    {
        self::$body = $body;
    }

    public static function send($to, $from, $subject)
    {
        if(self::$body == null){
            return false;
        }
        $message = '<html><body>';
        $message .= self::$body;
        $message .= "</body></html>";

        self::$body = null;


        $headers = "From: " . strip_tags($from) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        return mail($to, $subject, $message, $headers);

    }

} 