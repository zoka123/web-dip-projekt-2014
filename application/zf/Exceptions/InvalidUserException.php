<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 22.05.14.
 * Time: 18:30
 */
class InvalidUserException extends Exception
{
    public function __construct($message = "Korisnik ne postoji", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}