<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 22.05.14.
 * Time: 18:30
 */
class LoginRequiredException extends Exception
{
    public function __construct($message = "Morate se prijaviti", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}