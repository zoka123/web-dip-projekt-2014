<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 20.05.14.
 * Time: 21:38
 */
class Session
{

    public static function init()
    {
        session_start();
    }

    public static function all()
    {
        return $_SESSION;
    }

    public static function  end()
    {
        session_destroy();
    }

    public static function  set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function  get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }

    public static function  remove($key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        } else {
            return false;
        }
    }

    public static function flash($key, $value = null)
    {
        if ($value === null) {
            if (!isset($_SESSION['flash'][$key])) {
                return false;
            }
            $ret = $_SESSION['flash'][$key];
            unset($_SESSION['flash'][$key]);

            return $ret;
        } else {
            $_SESSION['flash'][$key] = $value;
        }
    }

}