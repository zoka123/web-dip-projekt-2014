<?php

class View
{

    private static $twig;

    public static function init()
    {
        $loader = new Twig_Loader_Filesystem(Config::base() . Config::get("template_path"));
        self::$twig = new Twig_Environment($loader);

        self::$twig->addGlobal("title", "ePrekršaji 2014");
        self::$twig->addGlobal("base_url", Config::get("base_url"));
        self::$twig->addGlobal("upload_base", Config::get("upload_base"));
        self::$twig->addGlobal("session", $_SESSION);
        self::$twig->addGlobal("cookie", $_COOKIE);
        self::$twig->addGlobal("time", VirtualTime::vtime());

        self::$twig->addFunction(
            new \Twig_SimpleFunction('css', function ($asset) {
                return Asset::css($asset);
            })
        );

        self::$twig->addFunction(
            new \Twig_SimpleFunction('js', function ($asset) {
                return Asset::js($asset);
            })
        );

        self::$twig->addFunction(
            new \Twig_SimpleFunction('form_input', function (
                $type = "text",
                $label = null,
                $name = null,
                $value = null,
                $additional = null
            ) {

                return Form::input($type, $label, $name, $value, $additional);
            })
        );

        self::$twig->addFunction(
            new \Twig_SimpleFunction('form_open', function (
                $action,
                $method,
                $additional = null
            ) {

                return Form::open($action, $method, $additional);
            })
        );

        self::$twig->addFunction(
            new \Twig_SimpleFunction('isAjax', function (){
                if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                   return true;
                } else {
                    return false;
                }
            })
        );

    }

    public static function render()
    {

        if (!self::$twig) {
            self::init();
        }

        echo call_user_func_array(array(self::$twig, 'render'), func_get_args());
    }

} 