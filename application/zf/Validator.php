<?php

class Validator
{

    private $data;
    private $rules;
    private $errors;

    private $errorMessages = array(
        "required" => "Polje # je obavezno",
        "min" => "Vrijednost polja # mora biti najmanje % znakova",
        "max" => "Vrijednost polja # mora biti najviše % znakova",
        "alpha" => "Vrijednost polja # mora sadržavati samo slova",
        "email" => "Vrijednost polja # nije ispravna email adresa",
        "numeric" => "Vrijednost polja # mora sadržavati samo brojeve",
        "capitalized" => "Vrijednost polja # mora započeti s velikim slovom",
    );

    private function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
    }

    private static function min($data, $length)
    {
        return (strlen($data) >= $length);
    }

    private static function max($data, $length)
    {
        return (strlen($data) <= $length);
    }

    private static function alpha($data)
    {
        return true;
    }

    private static function numeric($data)
    {
        return (is_numeric($data));
    }

    private static function capitalized($data)
    {
        return ($data[0] = strtoupper($data[0]));
    }

    private static function required($data)
    {
        return !empty($data);
    }

    private static function email($data)
    {
        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$data)) {
            return false;
        }
        return true;
    }

    public static function make($data, $rules)
    {
        return new Validator($data, $rules);
    }

    public function fails()
    {
        $errors = array();
        foreach ($this->rules as $input => $ruleSet) {
            $rules = explode("|", $ruleSet);
            foreach ($rules as $rule) {
                //echo "checking $input for $rule <br/>";

                $rule = explode(":", $rule);

                if(isset($this->data[$input])){
                    $params = array(
                        $this->data[$input]
                    );
                } else {
                    $params = array(
                        null
                    );
                }


                if (isset($rule[1])) {
                    $params[] = $rule[1];
                }

                if (
                !call_user_func_array(array($this, $rule[0]), $params)
                ) {
                    $errorMsg = $this->errorMessages[$rule[0]];
                    if(strpos($errorMsg,"%")){
                        $errorMsg = str_replace("%", $rule[1], $errorMsg);
                    }
                    $errors[$input][] = $errorMsg;
                }

            }
        }

        if (empty($errors)) {
            return false;
        } else {
            $this->errors = $errors;
            return true;
        }
    }


    public function errors()
    {
        return $this->errors;
    }

}