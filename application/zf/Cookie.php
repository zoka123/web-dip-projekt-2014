<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 22.05.14.
 * Time: 21:02
 */
class Cookie
{

    public static function set($key, $value, $expire = null, $path=null, $domain = null)
    {
        if($expire == null){
            $expire = time() + 60*60*24*30;
        }
        setcookie($key, $value, $expire, $path, $domain);

    }

    public static function get($key)
    {
        if(isset($_COOKIE[$key])){
            return $_COOKIE[$key];
        } else {
            return false;
        }
    }

    public static function remove($key){
        setcookie($key, "", time()-3600);
    }

} 