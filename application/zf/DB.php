<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 19.05.14.
 * Time: 00:22
 */
class DB
{

    /**
     * @var PDO
     */
    private static $dbh = null;

    public static function connect()
    {
        self::$dbh = self::getPDO();
    }

    public static function dbh()
    {
        return self::getPDO();
    }

    public static function getPDO()
    {

        if (!self::$dbh) {
            $dbname = Config::get("database");
            $host = Config::get("host");
            $username = Config::get("username");
            $password = Config::get("password");

            self::$dbh = new PDOTester("mysql:dbname={$dbname};host={$host};charset=utf8", $username, $password);
            self::$dbh->query("SET names utf8");
        }
        return self::$dbh;
    }
}