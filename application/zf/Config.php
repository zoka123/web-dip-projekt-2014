<?php

class Config
{

    private static $config = array();

    public static function init()
    {
        Session::init();
        self::cache();
        if(true || self::get("app_mode") != "development"){
            set_error_handler("zf_error_handler");
            set_exception_handler("zf_exception_handler");
            //register_shutdown_function('errorHandler');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
        }

        VirtualTime::init();

    }

    private static function cache()
    {
        $cacheFile = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . "config.php";
        //if (!is_file($cacheFile)) {
            $root = dirname(__DIR__) . DIRECTORY_SEPARATOR;
            $dir = $root . 'config';
            $files = scandir($dir);

            $config = array();
            foreach ($files as $file) {
                // If it is a conf files
                if (preg_match("/^.+(\.conf\.php)$/", $file)) {
                    $currentConfig = include($dir . DIRECTORY_SEPARATOR . $file);
                    $config += $currentConfig;
                }
            }

            file_put_contents($cacheFile, "<?php \n return " . var_export($config, true) . ";");
        //}
        self::$config = include($cacheFile);
    }

    public static function get($key)
    {
        return self::$config[$key];
    }

    public static function base()
    {
        return dirname(dirname(__DIR__));
    }

    public static function isDevelopment(){
        return self::get("app_mode") == "development";
    }


} 