<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 19.05.14.
 * Time: 00:20
 */
class Auth
{

    private static $key = "auth";

    public static function check()
    {
        return Session::get("user");
    }

    public static function logout()
    {
        Log::add(Log::$LOG_TYPES["LOG_TYPE_LOGOUT"]);
        Session::remove("user");
        //Cookie::remove("email");
        Session::end();
    }

    public static function attempt($userData)
    {

        if ($user = User::attempt($userData)) {
            if (count($user) != 1) {
                return false;
            } else {
                $user = $user[0];
            }
            Session::set(
                "user",
                array(
                    "user_role_id" => $user["user_role_id"],
                    "user_id" => $user["user_id"],
                    "first_name" => $user["first_name"],
                    "last_name" => $user["last_name"],
                    "email" => $user["email"]
                ));
            Cookie::set("last_email", $user['email']);
            return true;
        } else {
            return false;
        }
    }


}