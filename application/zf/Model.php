<?php

/**
 * Class Model
 * Abstract class for implementing custom ORM solution
 */
abstract class Model
{

    /**
     * Editable fields for accesing in direct model property setting
     * @var array
     *
     */
    public static $editable = array();
    public static $defaultAttributes = array("created_at", "modified_at", "deleted_at");

    public static $timeFilterColumn = "created_at";

    const MODEL_MODE_ARRAY = 1;
    const MODEL_MODE_OBJECT = 2;

    public $properties = array();

    protected static $primaryKey = "id";

    /**
     * Table to which model is bound
     * @var string
     */
    protected static $table = "";

    public function __construct()
    {
        DB::connect();

//      Lazy load all attributes as empty objects
        if (!count(static::$attributes)) {
            self::loadAttributes();
        }

        $this->properties = array();
//      Clean set of new attributes
        foreach (static::$attributes as $key => $attribute) {
            $this->properties[$key] = clone($attribute);
        }
    }

    public function formatAttr($attr)
    {

        if ($attr->data_type == "datetime") {
            return date("d.m.Y - H:i", strtotime($attr->value));
        }

        return $attr->value;
    }

    public function __get($attribute)
    {
        $getter = "get_" . $attribute;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (array_key_exists($attribute, $this->properties)) {

            return $this->formatAttr($this->properties[$attribute]);
        } else {
            throw new Exception("Attribute {$attribute} of class " . get_called_class() . " does not exist");
        }
    }

    public function prepareAttr($attribute, $value)
    {

        if ($this->properties[$attribute]->data_type == "datetime" && !empty($value)) {
            $value = datetime($value);
        }

        if ($this->properties[$attribute]->data_type == "timestamp" && !empty($value)) {
            $value = datetime($value);
        }

        $this->properties[$attribute]->value = $value;
    }

    public function __set($attribute, $value)
    {
        if (array_key_exists($attribute, $this->properties)) {
            $this->prepareAttr($attribute, $value);
        } else {
            throw new Exception("Attribute {$attribute} of class " . get_called_class() . " does not exist");
        }

    }

    public
    function setValues($values)
    {
        foreach ($values as $attribute => $value) {
            $this->$attribute = $value;
        }
    }

    public function propertiesArray(){
        $return = array();
        foreach ($this->properties as $attribute => $value) {
            $return[$attribute] = $value->value;
        }
        return $return;
    }

    public
    function getValues()
    {
        foreach ($this->properties as $attribute => $value) {
            echo $attribute . " : " . $value->value . PHP_EOL;
        }
    }

    /**
     * Get info about table
     */
    protected
    static function loadAttributes()
    {
        $stmt = DB::dbh()->prepare(
            'SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = :db_name AND TABLE_NAME = :tbl_name;'
        );
        $stmt->execute(array('db_name' => Config::get("database"), 'tbl_name' => static::$table));

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $attribute = new Attribute($row);
            static::$attributes[$attribute->name] = $attribute;
        }
    }


    /**
     * @param string $table
     */
    public function setTable($table)
    {
        static::$table = $table;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return static::$table;
    }

    protected static function buildQuery($key = null, $params = null, $attributes = null)
    {
        $sql = "SELECT ";

        if (isset($params["count"]) && $params["count"] == true) {
            $sql .= " COUNT(*) as count ";
        } else {
            if (empty($attributes)) {
                $attributes = static::$attributes;
                foreach ($attributes as $attribute) {
                    $sql .= "`{$attribute->name}`,";
                }
            } else {
                foreach ($attributes as $attribute) {
                    $sql .= "`{$attribute}`,";
                }
            }

            $sql = substr($sql, 0, -1);
        }

        $sql .= " FROM `" . static::$table . "` WHERE 1 ";

        if (isset($key) && $key !== null) {
            $sql .= " AND `" . static::$primaryKey . "` = :key ";
        }

        if (isset($params['where']) && $params["where"] !== null) {
            $sql .= "AND {$params['where']} ";
        }

        if (isset($params['order']) && $params["order"] !== null) {
            $sql .= " ORDER BY {$params['order']} ";
        }

        if (isset($params['limit']) && $params["limit"] !== null) {
            $sql .= " LIMIT {$params['limit']} ";
        }

        return $sql;
    }


    protected static function getEditableAttributes()
    {
        $return = array();
        if (count(static::$editable)) {

            foreach (static::$editable as $attr) {
                $return[$attr] = static::$attributes[$attr];
            }
            foreach (static::$defaultAttributes as $attr) {
                $return[$attr] = static::$attributes[$attr];
            }
            return $return;
        } else {
            return null;
        }
    }

    protected static function buildUpdateQuery()
    {
        $sql = "UPDATE `" . static::$table . "` SET";

        $attributes = self::getEditableAttributes();
        foreach ($attributes as $attribute) {
            $sql .= " `{$attribute->name}` = :" . $attribute->name . ",";
        }
        $sql = substr($sql, 0, -1);

        $sql .= " WHERE `" . static::$primaryKey . "` = :key ";

        return $sql;
    }

    public static function buildInsertQuery()
    {
        $sql = "INSERT into `" . static::$table . "` SET";

        $attributes = self::getEditableAttributes();
        foreach ($attributes as $attribute) {
            $sql .= " `{$attribute->name}` = :" . $attribute->name . ",";
        }
        $sql = substr($sql, 0, -1);

        return $sql;
    }

    /**
     * @param $key
     * @param null $attributes
     */
    public function load($key)
    {
        $stmt = DB::dbh()->prepare(self::buildQuery($key));
        $stmt->execute(array(":key" => $key));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!empty($result) && is_array($result)) {
            foreach ($result as $attribute => $value) {
                $this->properties[$attribute]->value = $value;
            }
        } else {
            throw new Exception("Nema entiteta " . static::$table . ". ID: " . $key);
        }
    }

    public static function all($mode = self::MODEL_MODE_ARRAY, $params = null, $includeDeleted = false)
    {
        static::loadAttributes();

        if (!$includeDeleted) {
            if (isset($params["where"]) && !empty($params["where"])) {
                $params["where"] .= " AND deleted_at is null";
            } else {
                $params["where"] = " deleted_at is null";
            }
        }

        $stmt = DB::dbh()->prepare(self::buildQuery(null, $params));

        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!$results) {
            debug($stmt->getSQL());
        }

        if ($mode === self::MODEL_MODE_OBJECT) {
            $return = array();
            $class = get_called_class();
            foreach ($results as $result) {
                $model = new $class;
                $model->setValues($result);
                $return[] = $model;
            }
            return $return;
        } else {
            return $results;
        }

    }

    public static function keyValue($key, $value)
    {
        $columns = array($key, $value);
        $results = static::select($columns);

        $return = array();

        foreach ($results as $result) {
            $return[$result[$key]] = $result[$value];
        }
        return $return;
    }

    public static function select($columns, $mode = self::MODEL_MODE_ARRAY, $params = null, $includeDeleted = false)
    {
        static::loadAttributes();

        if (!$includeDeleted) {
            if (isset($params["where"]) && !empty($params["where"])) {
                $params["where"] .= " AND deleted_at is null";
            } else {
                $params["where"] = " deleted_at is null";
            }
        }

        $stmt = DB::dbh()->prepare(self::buildQuery(null, $params, $columns));

        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!$results) {
            debug($stmt->getSQL());
        }

        if ($mode === self::MODEL_MODE_OBJECT) {
            $return = array();
            $class = get_called_class();
            foreach ($results as $result) {
                $model = new $class;
                $model->setValues($result);
                $return[] = $model;
            }
            return $return;
        } else {
            return $results;
        }

    }

    public static function count($params = array(), $includeDeleted = false)
    {
        static::loadAttributes();

        unset($params["order"]);
        unset($params["limit"]);
        $params["count"] = true;

        if (!$includeDeleted) {
            if (isset($params["where"]) && !empty($params["where"])) {
                $params["where"] .= " AND deleted_at is null";
            } else {
                $params["where"] = " deleted_at is null";
            }
        }

        if (Input::get("date_start") && Input::get("date_end")) {
            $start = datetime(Input::get("date_start"));
            $end = datetime(Input::get("date_end"));
            $timeFilterColumn = static::$timeFilterColumn;
            $filter = " {$timeFilterColumn} >= \"" . $start . "\" AND {$timeFilterColumn} <= \"" . $end ."\"";

            if (isset($params["where"]) && !empty($params["where"])) {
                $filter = " AND " . $filter;
            }

            if(isset($params["where"])){
                $params["where"] .=  $filter;
            } else {
                $params["where"] =  $filter;
            }
        }


        $stmt = DB::dbh()->prepare(self::buildQuery(null, $params));

        $stmt->execute();

        $results = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$results) {
            debug($stmt->getSQL());
        }

        return $results["count"];

    }


    /**
     * Processing model before saving
     */
    public function before_save()
    {

    }

    /**
     * Processing newly inserted model before saving
     */
    public function before_save_new()
    {

    }

    /**
     * Processing attributes after loading model
     */
    public function after_load()
    {

    }

    /**
     * For overriding
     * @return void
     */
    public function save()
    {
        $action = "insert";
        $pk = static::$primaryKey;

        $id = $this->$pk;
        if (!isset($id)) {
            $stmt = DB::dbh()->prepare(self::buildInsertQuery());
        } else {
            $action = "update";
            $stmt = DB::dbh()->prepare(self::buildUpdateQuery());
        }

        $attributes = self::getEditableAttributes();

        $types = array(
            "varchar" => PDO::PARAM_STR,
            "char" => PDO::PARAM_STR,
            "text" => PDO::PARAM_STR,
            "decimal" => PDO::PARAM_STR,
            "timestamp" => PDO::PARAM_INT,
            "tinyint" => PDO::PARAM_STR,
            "int" => PDO::PARAM_INT,
            "date" => PDO::PARAM_STR,
            "datetime" => PDO::PARAM_STR,
        );

        foreach ($attributes as $attribute) {
            $type = $types[$attribute->data_type];
            $attrName = ':' . $attribute->name;
//            debug("BINDING ".$attrName . " TO " . $this->properties[$attribute->name]->value . PHP_EOL );
            $value = $this->properties[$attribute->name]->value;
            if (!isset($value) && $value != 0) {
                $value = null;
            }
            $stmt->bindValue($attrName, $value, $type);
        }

        $null = null;
        $stmt->bindValue(
            ':deleted_at',
            $null,
            PDO::PARAM_NULL
        );

        // Is it insert or update?
        // Set timestamps
        $time = datetime();
        $createdTime = datetime();
        if(!empty($this->properties["created_at"]->value)){
            $createdTime = datetime($this->created_at);
        }


        if (isset($id)) {
            $stmt->bindValue(
                ':key',
                $id,
                $types[$this->properties[$pk]->data_type]
            );

            $stmt->bindValue(
                ':created_at',
                $this->created_at,
                PDO::PARAM_STR
            );

            $stmt->bindValue(
                ':modified_at',
                $time,
                PDO::PARAM_STR
            );
        } else {
            $stmt->bindValue(
                ':created_at',
                $createdTime,
                PDO::PARAM_STR
            );
            $stmt->bindValue(
                ':modified_at',
                $null,
                PDO::PARAM_STR
            );
        }

        if (isset($this->properties["deleted_at"]) && !empty($this->properties["deleted_at"]->value)) {
            $stmt->bindValue(
                ':deleted_at',
                $time,
                PDO::PARAM_STR
            );
        }

        $ret = $stmt->execute();

        if ($ret) {
            if ($action == "insert") {
                $ret = DB::getPDO()->lastInsertId();
                if(static::$table != Log::$table){
                    Log::add(Log::$LOG_TYPES["DATABASE_OPERATION"], print_r($stmt->getSQL(), true), "INSERT");
                }
                return $ret;
            } else {
                if(static::$table != Log::$table){
                    Log::add(Log::$LOG_TYPES["DATABASE_OPERATION"], print_r($stmt->getSQL(), true), "UPDATE");
                }
            }
            return true;
        } else {
            debug($stmt->getSQL());
            debug($stmt->errorInfo());
            return false;
            throw new Exception();
        }
    }


    public function delete($id)
    {
        $this->load($id);
        $this->deleted_at = VirtualTime::vtime();
        return $this->save();
    }

    public static function findWhere($filters, $customFilters = null)
    {
        $sql = "SELECT *  FROM `" . static::$table . "` WHERE ";

        $first = true;
        foreach ($filters as $name => $value) {
            if ($first) {
                $first = false;
            } else {
                $sql .= " AND ";
            }
            if (!is_int($value) && !is_float($value)) {
                $sql .= " $name = \"$value\" ";
            } else {
                $sql .= " $name = $value ";
            }
        }


        if (!empty($customFilters)) {
            if (empty($filters)) {
                $sql .= $customFilters;
            } else {
                $sql .= " AND " . $customFilters;
            }
        }

        $sth = DB::dbh()->query($sql);
        $result = $sth->fetchAll();

        return $result;
    }


    public function getKey()
    {
        return $this->properties[static::$primaryKey]->value;
    }

    public function fill($data)
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }
    }

    public static function makeKeyValuePairs($keyAttr, $valueAttr, $data)
    {
        $return = array();

        foreach ($data as $row) {
            $return[$row[$keyAttr]] = $row[$valueAttr];
        }

        return $return;
    }

}