<?php
/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 22.05.14.
 * Time: 19:55
 */

class Redirect {

    public static function to($url){
        header("Location: {$url}");
    }

    public static function route($route){
        $route = Config::get("base_url").$route;
        header("Location: {$route}");
    }
}