<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 22.05.14.
 * Time: 02:14
 */
class Form
{

    public static function input($type = "text", $label = null, $name = null, $value = null, $additional)
    {
        $ret = "";
        if ($label) {
            $ret .= '<label>' . $label;
        }
        $ret .= '<input type="' . $type . '" ';

        if ($name) {
            $ret .= 'name="' . $name . '" ';
        }
        if ($value) {
            $ret .= 'value="' . $value . '" ';
        }
        if ($label) {
            $ret .= 'placeholder="' . $label . '" ';
        }

        if ($additional) {
            $ret .= ' ' . $additional . ' ';
        }

        $ret .= "/>";

        if ($label) {
            $ret .= '</label>';
        }

        return $ret;
    }

    public static function submit($value, $additional = null)
    {
        $ret = '<button name="submit" type="submit" ';
        if ($additional) {
            $ret .= ' ' . $additional . ' ';
        }
        $ret .= ">" . $value . "</button>";

        return $ret;

    }

    public static function open($action, $method, $additional = null)
    {

        $ret = '<form action="' . $action . '" method="' . $method . '" ';

        if ($additional) {
            $ret .= ' ' . $additional . ' ';
        }

        $ret .= ">\n";
        $ret .= '<input type="hidden" name="form_timestamp" value="'.time().'">';

        return $ret;
    }

    public static function close()
    {
        return "</form>";
    }

    public static function processInputs(&$inputs, $object){
        foreach($inputs as &$input){
            $input["value"] = $object->{$input["name"]};

            if ($input["type"] == "checkbox") {
                if ($input["value"] == 1) {
                    $input["additional"] .= " checked";
                }
                $input["value"] = "enabled";
            }
        }
    }

} 