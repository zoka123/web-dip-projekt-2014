<?php


class zf_User extends Model
{
    protected static $table = "user";

    public static $editable = array(
        "user_role_id",
        "oib",
        "first_name",
        "last_name",
        "email",
        "password",
        "birthdate",
        "address",
        "description",
        "last_login_at",
        "enabled",
        "attempts",
    );

    public static function resetLoginAttempt($user){
        $sql = "UPDATE " . static::$table . " set attempts = 0, last_login_at = NOW() where email = \"" . $user["email"] . "\"";
        DB::getPDO()->query($sql);
    }

    public static function addLoginAttempt($user){
        //last_login_at is not NULL and
        $sql = "UPDATE " . static::$table . " set attempts = coalesce(attempts + 1,0), enabled = IF(attempts > 2, 0, enabled) where email = \"" . $user["email"]  . "\"";
        DB::getPDO()->query($sql);
    }

    public static function attempt($userData)
    {
        $userData["enabled"] = 1;
        $result = self::findWhere($userData);
        if(count($result) == 1){
            Log::add(Log::$LOG_TYPES["LOG_TYPE_SUCCESSFUL_LOGIN"], print_r($userData, true));
            self::resetLoginAttempt($userData);
            return $result;
        } else {
            self::addLoginAttempt($userData);
            Log::add(Log::$LOG_TYPES["LOG_TYPE_BAD_LOGIN"], print_r($userData, true));
            return $result;
        }
    }


}