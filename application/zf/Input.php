<?php

class Input
{

    public static function get($key)
    {
        $inputs = array_merge($_POST,$_GET);
        if(isset($inputs[$key])){
            return self::sanitize($inputs[$key]);
        } else {
            return false;
        }
    }

    public static function all()
    {

        $ret = array_merge($_POST,$_GET);
        unset($ret['r']);
        unset($ret['submit']);
        unset($ret['form_timestamp']);
        unset($ret['reset']);
        foreach($ret as &$request){
            $request = self::sanitize($request);
        }
        return $ret;
    }

    /**
     * http://css-tricks.com/snippets/php/sanitize-database-inputs/
     * @param $input
     * @return mixed
     */
    private static function cleanInput($input) {

        $search = array(
            '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
            '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
            '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        );

        $output = preg_replace($search, '', $input);
        return $output;
    }


    /**
     * http://css-tricks.com/snippets/php/sanitize-database-inputs/
     * @param $input
     * @return string
     */
    private static function sanitize($input) {
        if (is_array($input)) {
            foreach($input as $var=>$val) {
                $output[$var] = self::cleanInput($val);
            }
        }
        else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }
            $input  = self::cleanInput($input);
            $output = $input;
        }
        return $output;
    }

    public static function except($keys)
    {

    }

} 