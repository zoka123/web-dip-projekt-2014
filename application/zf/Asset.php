<?php

class Asset
{

    public static function js($path)
    {
        $path = Config::get("base_url")  . $path;

        return '<script src="' . $path . '"></script>';
    }

    public static function css($path)
    {
        $path = Config::get("base_url") . $path;

        return '<link href="' . $path . '" rel="stylesheet" type="text/css" />';
    }

} 