<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 19.05.14.
 * Time: 00:20
 * Source: http://upshots.org/php/php-seriously-simple-router
 */
class Router
{

    private static $routes = array();

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function add($pattern, $callback)
    {
        //Mask route as reqex
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';
        self::$routes[$pattern] = $callback;
    }

    public static function init()
    {
        // Get route as param since we processed it with htaccess
        $uri = (isset($_GET['r']) ? $_GET['r'] : "/");

        // Iterate over routes and find match
        foreach (self::$routes as $pattern => $callback) {
            if (preg_match($pattern, $uri, $params)) {

                // First match is full url, we don't need it
                unset($params[0]);

                //If route is closure, call it
                if (is_callable($callback)) {
                    return call_user_func_array($callback, array_values($params));
                } // If it's controller object, instantiate it and call action
                elseif (strpos($callback, "@")) {
                    // Extract controller and action name
                    list($controller, $action) = explode("@", $callback);

                    // Call method of object
                    $reflectionMethod = new ReflectionMethod($controller, $action);
                    return $reflectionMethod->invokeArgs(new $controller, $params);
                }
            }
        }

        // Route not found
        return View::render("error.twig.html", array("message" => "404", "error" => "STRANICA NIJE PRONAĐENA"));
        Error::scream(404);
    }

    public static function route($key){
        foreach (self::$routes as $pattern => $callback){
            if($callback == $key){
                $pattern = str_replace(array('\\','/^','$/'), "", $pattern);
                return $pattern;
            }
        }
    }

} 