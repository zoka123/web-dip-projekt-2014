<?php

/**
 * Created by PhpStorm.
 * User: Zoka
 * Date: 20.05.14.
 * Time: 23:24
 */
class Attribute
{

    public $name;
    public $value;
    public $position;
    public $default;
    public $notNull;
    public $data_type;
    public $col_type;
    public $char_max_length;

    public function __construct($arr_col)
    {
        $this->name = $arr_col['COLUMN_NAME'];
        $this->position = $arr_col['ORDINAL_POSITION'];
        $this->default = $arr_col['COLUMN_DEFAULT'];
        $this->notNull = ($arr_col['IS_NULLABLE'] == "YES" ? false : true);
        $this->data_type = $arr_col['DATA_TYPE'];
        $this->col_type = $arr_col['COLUMN_TYPE'];
        $this->char_max_length = $arr_col['CHARACTER_MAXIMUM_LENGTH'];
    }

} 