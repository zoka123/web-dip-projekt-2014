<?php

class AdminController extends Controller
{

    public function __construct()
    {
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }

        $this->menu = "admin-menu";
    }

    public function index(){
        $user = Session::get("user");

        if($user["user_role_id"] == User::MODERATOR){
            //User is moderator
            Redirect::route("moderator");
        } else if($user["user_role_id"] == User::REGISTERED){
            //Default user
            Redirect::route("registered");
        }

        self::checkPermissions($user,array(User::ADMIN));

        View::render('admin/dashboard.twig.html', array(
                "menu" => $this->menu
            ));

    }

    public function ending(){
        header('Content-Type: application/json');

        $return = Violation::getEndingToday();
        foreach($return as &$ret){
            $ret["created"] = date("d.m.Y H:i", strtotime($ret["created"]));
            $ret["ending"] = date("d.m.Y H:i", strtotime($ret["ending"]));
        }
        exit(json_encode($return));
    }

    public function endingFuture(){
        header('Content-Type: application/json');

        $return = Violation::getEndingFuture();
        foreach($return as &$ret){
            $ret["created"] = date("d.m.Y H:i", strtotime($ret["created"]));
            $ret["ending"] = date("d.m.Y H:i", strtotime($ret["ending"]));
        }
        exit(json_encode($return));
    }

    public function cities()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $columns = array(
            "city_id" => "#",
            "city_zip_code" => "Poš. broj",
            "name" => "Naziv",
            "enabled" => "Status",
            "county_id" => "Županija",
        );

        $paginator = new Paginator();
        $paginator->setColumns($columns);
        $count = City::count();
        $paginator->setCount($count);
        $paginator->prepare();


        $params = $paginator->getSQLParams();

        $cities = City::select(array_keys($columns), User::MODEL_MODE_ARRAY, $params);

        $counties = County::keyValue("county_id","name");
        foreach($cities as &$city){
            $city["county_id"] = $counties[$city["county_id"]];
            if (array_key_exists("enabled", $columns)) {
                if ($city["enabled"] == 1) {
                    $city["enabled"] = '<i class="fa fa-check green-bg"></i>';
                } else {
                    $city["enabled"] = '<i class="fa fa-times red-bg"></i>';
                }
            }
        }

        $pagination = $paginator->getPaginationLinks(Input::all());

        View::render('skeleton/crud-table.twig.html', array(
                'menu' => $this->menu,
                'base' => Config::get("base_url") . "cities",
                'headers' => $paginator->columns,
                'key' => "city_id",
                'data' => $cities,
                'pagination' => $pagination,
                'add_link' => "city/add",
                'icons' => array(
                    "fa-edit" => "city/edit/",
                )
            ));

    }

    public function cityEdit($id){

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $city = new City();
        $city -> load($id);

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => ' class="" required ',),
                array("label" => "Županija", "type" => "select", "class" => 'chosen-select', "name" => "county_id", "additional" => '', "options" => County::keyValue("county_id", "name")),
                array("label" => "Poš. broj", "type" => "text", "name" => "city_zip_code", "additional" => "required",),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
            );

            Form::processInputs($inputs, $city);

            return View::render("city/city-form.twig.html", array("inputs" => $inputs, "city_id" => $id, "delete_link" => "city/delete/".$id));
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
                "city_zip_code" => "required",
                "county_id" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {
                $city -> fill($data);

                $city->enabled = (!Input::get("enabled") ? 0 : 1);
                debug($city);
                if ($id = $city->save()) {
                    $response = array("status" => 1, "message" => 'Grad je ažuriran');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }

    public function cityAdd(){

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $city = new City();

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => ' class="" required ',),
                array("label" => "Županija", "type" => "select", "class" => 'chosen-select', "name" => "county_id", "additional" => '', "options" => County::keyValue("county_id", "name")),
                array("label" => "Poš. broj", "type" => "text", "name" => "city_zip_code", "additional" => "required",),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => '', "value" => "enabled"),
            );

            return View::render("city/city-form.twig.html", array("inputs" => $inputs, "form_action" => "city/add"));
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
                "city_zip_code" => "required",
                "county_id" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {
                $city -> fill($data);

                $city->enabled = (!Input::get("enabled") ? 0 : 1);

                if ($id = $city->save()) {
                    $response = array("status" => 1, "message" => 'Grad je dodan', "action" => 'externalCrudFormDisable();');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }

    public function cityDelete($id){

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $city = new City();
        if($city -> delete($id)){
            echo "1";
        } else {
            echo "0";
        }
    }

} 