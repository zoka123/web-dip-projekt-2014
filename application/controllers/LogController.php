<?php

class LogController extends Controller
{

    public function __construct()
    {
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }

        $this->menu = "admin-menu";
    }


    public function all()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $columns = array(
            "log_id" => "#",
            "query" => "Upit",
            "log_type" => "Tip",
            "user_id" => "Korisnik",
            "additional" => "Podaci",
            "created_at" => "Datum",
        );

        $paginator = new Paginator();
        $paginator->setColumns($columns);
        $count = Log::count();
        $paginator->setCount($count);
        $paginator->prepare();

        $logs = Log::select(array_keys($columns), User::MODEL_MODE_ARRAY, $paginator->getSQLParams());
        $users = User::customKeyValue();

        foreach($logs as &$log){
            $log["log_type"] = Log::getLogType($log["log_type"]);
            if(in_array($log["user_id"],array_keys($users))){
                $log["user_id"] = $users[$log["user_id"]];
            }
        }

        $pagination = $paginator->getPaginationLinks(Input::all());

        View::render(
            'skeleton/crud-table.twig.html',
            array(
                'menu' => "admin-menu",
                'base' => Config::get("base_url") . "logs",
                'headers' => $paginator->columns,
                'key' => "log_id",
                'data' => $logs,
                'show_time_filters' => 1,
                'pagination' => $pagination,
            )
        );

    }

}