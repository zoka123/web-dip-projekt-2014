<?php

class RegisteredController extends Controller
{

    public function __construct()
    {
        $user = Session::get("user");
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }
        self::checkPermissions($user,array(User::REGISTERED, User::ADMIN));

        $this->menu = "registered-menu";
    }

    public function index($id=null){
        $user = new User();
        if($id == null){
            $sessionUser = Session::get("user");
            $user->load($sessionUser["user_id"]);
        } else {
            $user->load($id);
        }
        $details = $user -> details();

        $violations = $user->violations();
        $complaints = $user->complaints();
        $complaintsStats = $user->complaintsStats();

        $js = '[\'Status\',\'#\'],';
        foreach($complaintsStats as $stat){
            $key = Complaint::$COMPLAINT_STATUS[$stat["complaint_status_id"]];
            $ret[$key] = $stat["count"];
            $js.= '[\'' . $key . '\', ' . $stat["count"] . '],';
        }

        $js = substr($js, 0, -1);

        View::render('registered/dashboard.twig.html', array(
                "menu" => $this->menu,
                "user" => $details,
                "violations" => array("data" => $violations, "base" => Config::get("base_url") . "api/violation-details/") ,
                "complaints" => array("data" => $complaints, "base" => Config::get("base_url") . "api/complaint-details/") ,
                "chart" => $js,
            ));
    }

} 