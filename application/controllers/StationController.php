<?php

class StationController extends Controller
{

    public function __construct()
    {
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }

        $this->menu = "admin-menu";
    }

    public function addOfficers($stationId)
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $station = new PoliceStation();
        $station->load($stationId);

        if (!Input::get("form_timestamp")) {
            $stationOfficers = $station->keyValueOfficers();
            $availableOfficers = PoliceOfficer::customKeyValue();

            $listOfficers = array();
            foreach ($availableOfficers as $k => $officer) {
                $selected = "false";
                if (in_array($k, array_keys($stationOfficers))) {
                    $selected = "true";
                }
                $listOfficers[$k] = array("label" => $officer, "selected" => $selected);
            }


            $selectParams = array(
                "options" => $listOfficers,
            );
            return View::render(
                "station/station-officers-form.twig.html",
                array("input" => $selectParams, "police_station" => $station->propertiesArray()));
        } else {

            DB::dbh()->beginTransaction();
            $officers = Input::get("stationOfficers");
            foreach( $officers as $officer){
                $officerInstance = new PoliceOfficer();
                $officerInstance->load($officer);
                $officerInstance->police_station_id = $stationId;
                if(!$officerInstance->save()){
                    DB::dbh()->rollBack();
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                    exit;
                }
            }
            if(!$station->clearOfficers($officers)){
                DB::dbh()->rollBack();
                $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške prilikom premještanja korisnika"));
                echo json_encode($response);
                exit;
            }
            DB::dbh()->commit();

            $response = array("status" => 1, "message" => 'Djelatnici su ažurirani');
            echo json_encode($response);
            exit;
        }
    }

    public function all()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $columns = array(
            "police_station_id" => "#",
            "county_id" => "Županija",
            "name" => "naziv",
            "address" => "Adresa",
            "telephone" => "Telefon",
            "enabled" => "Status",
        );

        $paginator = new Paginator();
        $paginator->setColumns($columns);
        $count = PoliceStation::count();
        $paginator->setCount($count);
        $paginator->prepare();

        $stations = PoliceStation::select(array_keys($columns), User::MODEL_MODE_ARRAY, $paginator->getSQLParams());

        $counties = County::keyValue("county_id", "name");
        foreach ($stations as &$station) {
            $station["county_id"] = $counties[$station["county_id"]];

        }

        $pagination = $paginator->getPaginationLinks(Input::all());

        $icons = array(
            "fa-edit" => "station/edit/",
            "fa-users" => "station/add-officers/",
            //                "fa-times" => "user/delete/",
        );

        if (array_key_exists("enabled", $columns)) {
            foreach ($stations as &$station) {
                if ($station["enabled"] == 1) {
                    $station["enabled"] = '<i class="fa fa-check green-bg"></i>';
                } else {
                    $station["enabled"] = '<i class="fa fa-times red-bg"></i>';
                }
            }
        }

        View::render(
            'skeleton/crud-table.twig.html',
            array(
                'menu' => "admin-menu",
                'base' => Config::get("base_url") . "stations",
                'headers' => $paginator->columns,
                'key' => "police_station_id",
                'add_link' => "station/add",
                'data' => $stations,
                'pagination' => $pagination,
                'icons' => $icons,
            )
        );

    }

    public function edit($id)
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $station = new PoliceStation();
        $station->load($id);

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => "required",),
                array("label" => "Adresa", "type" => "text", "name" => "address", "additional" => ''),
                array("label" => "Telefon", "type" => "phone", "name" => "telephone", "additional" => ''),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
                array("label" => "Županija", "type" => "select", "class" => 'chosen-select', "name" => "county_id", "additional" => '', "options" => County::keyValue("county_id", "name")),
            );

            Form::processInputs($inputs, $station);

            return View::render(
                "station/station-form.twig.html",
                array("inputs" => $inputs, "police_station_id" => $id, "delete_link" => "station/delete/" . $id)
            );
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
                "county_id" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $station->fill($data);
                $station->enabled = (!Input::get("enabled") ? 0 : 1);

                if ($id = $station->save()) {
                    $response = array("status" => 1, "message" => 'Postaja je ažurirana');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


    public function add()
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $station = new PoliceStation();

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => "required",),
                array("label" => "Adresa", "type" => "text", "name" => "address", "additional" => ''),
                array("label" => "Telefon", "type" => "phone", "name" => "telephone", "additional" => ''),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
                array("label" => "Županija", "type" => "select", "class" => 'chosen-select', "name" => "county_id", "additional" => '', "options" => County::keyValue("county_id", "name")),
            );

            return View::render(
                "station/station-form.twig.html",
                array("inputs" => $inputs, "form_action" => "station/add",)
            );
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
                "county_id" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $station->fill($data);
                $station->enabled = (!Input::get("enabled") ? 0 : 1);

                if ($id = $station->save()) {
                    $response = array("status" => 1, "message" => 'Postaja je dodana', "action" => 'externalCrudFormDisable();');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


    public function delete($id)
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $station = new PoliceStation();
        if ($station->delete($id)) {
            echo "1";
        } else {
            echo "0";
        }
    }


}