<?php

class UserController extends Controller
{

    public function __construct()
    {
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }
    }

    public function all()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $columns = array(
            "user_id" => "#",
            "oib" => "OIB",
            "first_name" => "Ime",
            "last_name" => "Prezime",
            "user_role_id" => "Razina",
            "email" => "Email",
            "enabled" => "Status",
            "last_login_at" => "Zadnja prijava",
            "attempts" => "Neuspj. prij.",
        );

        $paginator = new Paginator();
        $paginator->setColumns($columns);
        $count = User::count();
        $paginator->setCount($count);
        $paginator->prepare();

        $users = User::select(array_keys($columns), User::MODEL_MODE_ARRAY, $paginator->getSQLParams());

        $userRoles = UserRole::keyValue("user_role_id","name");
        foreach($users as &$user){
            $user["user_role_id"] = $userRoles[$user["user_role_id"]];
            $user["last_login_at"] = (empty($user["last_login_at"]) ? " - " : date("d.m.Y. - H:i", strtotime($user["last_login_at"])));

        }

        $pagination = $paginator->getPaginationLinks(Input::all());

        $icons = array(
            "fa-edit" => "user/edit/",
            "fa-user" => "user/",
            //                "fa-times" => "user/delete/",
        );

        if (array_key_exists("enabled", $columns)) {
            foreach ($users as &$user) {
                if ($user["enabled"] == 1) {
                    $user["enabled"] = '<i class="fa fa-check green-bg"></i>';
                } else {
                    $user["enabled"] = '<i class="fa fa-times red-bg"></i>';
                }
            }
        }

        View::render(
            'skeleton/crud-table.twig.html',
            array(
                'menu' => "admin-menu",
                'base' => Config::get("base_url") . "users",
                'headers' => $paginator->columns,
                'key' => "user_id",
                'add_link' => "user/add",
                'data' => $users,
                'pagination' => $pagination,
                'icons' => $icons,
            )
        );

    }

    public function edit($id)
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $user = new User();
        $user->load($id);

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "OIB", "type" => "text", "name" => "oib", "additional" => ' class="" required ',),
                array("label" => "Ime", "type" => "text", "name" => "first_name", "additional" => "required",),
                array("label" => "Prezime", "type" => "text", "name" => "last_name", "additional" => "required",),
                array("label" => "Email", "type" => "email", "name" => "email", "additional" => ' class="" required ',),
                array(
                    "label" => "Lozinka",
                    "type" => "password",
                    "name" => "password",
                    "additional" => ' pattern=".{6,}" ',
                    "validationMessage" => "Minimalno 6 znakova!"
                ),
                array(
                    "label" => "Datum rođenja",
                    "type" => "text",
                    "name" => "birthdate",
                    "additional" => 'required class="datepicker"'
                ),
                array("label" => "Adresa", "type" => "text", "name" => "address", "additional" => 'required'),
                array("label" => "Opis", "type" => "text", "name" => "description", "additional" => ''),
                array("label" => "Broj neuspj. prijava", "type" => "number", "name" => "attempts", "additional" => ' min="0" '),
                array("label" => "Telefon", "type" => "phone", "name" => "telephone", "additional" => ''),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
                array("label" => "Uloga", "type" => "select", "class" => 'chosen-select', "name" => "user_role_id", "additional" => '', "options" => UserRole::keyValue("user_role_id", "name")),
            );

            Form::processInputs($inputs, $user);

            return View::render(
                "users/user-edit-form.twig.html",
                array("inputs" => $inputs, "user_id" => $id, "delete_link" => "user/delete/" . $id)
            );
        } else {
            $data = Input::all();

            $validator = Validator::make($data, User::$VALIDATION_RULES);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $checkOib = User::findWhere(array("oib" => $data["oib"]), " deleted_at IS NULL AND user_id not in (" . $user->user_id . ") ");
                $checkEmail = User::findWhere(
                    array("email" => $data["email"]),
                    " deleted_at IS NULL AND user_id not in (" . $user->user_id . ") "
                );

                if ((int)(count($checkOib) != 0)) {
                    $response = array("status" => 0, "msg" => array("oib" => array("Vrijednost polja # je zauzeta")));
                    exit(json_encode($response));
                }
                if ((int)(count($checkEmail) != 0)) {
                    $response = array("status" => 0, "msg" => array("email" => array("Vrijednost polja # je zauzeta")));
                    exit(json_encode($response));
                }

                $user->fill($data);
                if (!Input::get("enabled")) {
                    $user->enabled = 0;
                } else {
                    $user->enabled = 1;
                }

                if ($id = $user->save()) {
                    $response = array("status" => 1, "message" => 'Korisnik je ažuriran');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }

    public function add()
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $user = new User();

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "OIB", "type" => "text", "name" => "oib", "additional" => ' class="" required ',),
                array("label" => "Ime", "type" => "text", "name" => "first_name", "additional" => "required",),
                array("label" => "Prezime", "type" => "text", "name" => "last_name", "additional" => "required",),
                array("label" => "Email", "type" => "email", "name" => "email", "additional" => ' class="" required ',),
                array(
                    "label" => "Lozinka",
                    "type" => "password",
                    "name" => "password",
                    "additional" => ' pattern=".{6,}" ',
                    "validationMessage" => "Minimalno 6 znakova!"
                ),
                array(
                    "label" => "Datum rođenja",
                    "type" => "text",
                    "name" => "birthdate",
                    "additional" => 'required class="datepicker"'
                ),
                array("label" => "Adresa", "type" => "text", "name" => "address", "additional" => 'required'),
                array("label" => "Opis", "type" => "text", "name" => "description", "additional" => ''),
                array("label" => "Broj neuspj. prijava", "type" => "number", "name" => "attempts", "additional" => ' min="0" '),
                array("label" => "Telefon", "type" => "phone", "name" => "telephone", "additional" => ''),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
                array("label" => "Uloga", "type" => "select", "class" => 'chosen-select', "name" => "user_role_id", "additional" => '', "options" => UserRole::keyValue("user_role_id", "name")),
            );

            return View::render(
                "users/user-edit-form.twig.html",
                array("inputs" => $inputs, "form_action" => "user/add" )
            );
        } else {
            $data = Input::all();
            $validator = Validator::make($data, User::$VALIDATION_RULES);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $checkOib = User::findWhere(array("oib" => $data["oib"]), " deleted_at IS NULL");
                $checkEmail = User::findWhere(
                    array("email" => $data["email"]),
                    " deleted_at IS NULL "
                );

                if ((int)(count($checkOib) != 0)) {
                    $response = array("status" => 0, "msg" => array("oib" => array("Vrijednost polja # je zauzeta")));
                    exit(json_encode($response));
                }
                if ((int)(count($checkEmail) != 0)) {
                    $response = array("status" => 0, "msg" => array("email" => array("Vrijednost polja # je zauzeta")));
                    exit(json_encode($response));
                }

                $user->fill($data);
                $user->enabled = (!Input::get("enabled") ? 0 : 1);

                if ($id = $user->save()) {
                    $response = array("status" => 1, "message" => 'Korisnik je dodan', "action" => 'externalCrudFormDisable();');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


    public function delete($id)
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN, User::MODERATOR));
        $user = new User();
        if ($user->delete($id)) {
            echo "1";
        } else {
            echo "0";
        }
    }




    /*
    public function all()
    {
        $activeUsers = User::all(
            User::$mode["ARRAY"],
            array("order" => "register_date DESC", "where" => "enabled = 1")
        );
        $inactiveUsers = User::all(
            User::$mode["ARRAY"],
            array("order" => "register_date DESC", "where" => "enabled = 0")
        );

        return View::render(
            "users/user-list.twig.html",
            array(
                "user_types" => array("activeUsers" => $activeUsers, "inactiveUsers" => $inactiveUsers),
                "user_roles" => array(
                    "registered" => User::REGISTERED,
                    "administrator" => User::ADMIN,
                    "moderator" => User::MODERATOR
                )
            )
        );
    }

    public function details($id)
    {
        $user = Session::get("user");

        if (
            $user["id_user"] == $id || in_array($user["id_user_role"], array(User::MODERATOR, User::ADMIN))
        ) {
            $user = User::findWhere(array("OIB" => $id));

            //var_dump($user);

            return View::render("users/user-form-open.twig.html", array("user" => $user));

        } else {
            throw new NotAllowedException;
        }
    }

    public function edit($id)
    {
        $user = Session::get("user");

        if (!(
            $user["id_user"] == $id || in_array($user["id_user_role"], array(User::ADMIN))
        )
        ) {
            throw new NotAllowedException;
        }
        $user_data = User::findWhere(array("OIB" => $id));
        $user = new User();
        $user->load($id);
        if (!Input::get("form_timestamp")) {
            return View::render("users/user-edit-form-open.twig.html", array("user" => $user_data));
        } else {

            foreach (Input::all() as $k => $v) {
                $user->$k = $v;
            }

            if ($user->save()) {
                Redirect::route("user-" . $id . "/edit");
            } else {
                throw new Exception("Nije uspjelo ažuriranje korisnika");
            }
        }

    }

    public function toggle($id)
    {
        $user = Session::get("user");

        if (!(
        in_array($user["id_user_role"], array(User::ADMIN))
        )
        ) {
            throw new NotAllowedException;
        }

        $user = new User();
        $user->load($id);
        $user->enabled = 1 - $user->enabled;
        if ($user->save()) {
            Redirect::route("users");
        } else {
            throw new Exception("Nije uspjelo ažuriranje korisnika");
        }

    }


    public function login()
    {
        if (!Input::get("form_timestamp")) {
            return View::render("login/login.twig.html");
        } else {
            $data = Input::all();

            $rules = array(
                "email" => "required",
                "password" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                Session::flash("errors", array("text" => $validator->errors(), "class" => "alert"));

                return View::render("login/login.twig.html");
            } else {
                if (Auth::attempt($data)) {
                    echo "User logged in";
                } else {
                    throw new InvalidUserException();
                }

            }
        }
    }
*/

    public function logout()
    {
        Auth::logout();
        Redirect::route("login");
    }
}