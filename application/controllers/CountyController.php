<?php

class CountyController extends Controller
{

    public function __construct(){
    }

    public function index()
    {
    }


    public function allCounties()
    {
        $counties = County::all();
        echo json_encode($counties);

    }

    public function all()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $columns = array(
            "county_id" => "#",
            "name" => "Županija",
            "enabled" => "Status",
        );

        $paginator = new Paginator();
        $paginator->setColumns($columns);
        $count = PoliceStation::count();
        $paginator->setCount($count);
        $paginator->prepare();

        $counties = County::select(array_keys($columns), User::MODEL_MODE_ARRAY, $paginator->getSQLParams());

        $pagination = $paginator->getPaginationLinks(Input::all());

        $icons = array(
            "fa-edit" => "county/edit/",
            //                "fa-times" => "user/delete/",
        );

        if (array_key_exists("enabled", $columns)) {
            foreach ($counties as &$county) {
                if ($county["enabled"] == 1) {
                    $county["enabled"] = '<i class="fa fa-check green-bg"></i>';
                } else {
                    $county["enabled"] = '<i class="fa fa-times red-bg"></i>';
                }
            }
        }

        View::render(
            'skeleton/crud-table.twig.html',
            array(
                'menu' => "admin-menu",
                'base' => Config::get("base_url") . "counties",
                'headers' => $paginator->columns,
                'key' => "county_id",
                'add_link' => "county/add",
                'data' => $counties,
                'pagination' => $pagination,
                'icons' => $icons,
            )
        );

    }

    public function edit($id)
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $county = new County();
        $county->load($id);

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => "required",),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => '', "value" => "enabled"),
            );

            Form::processInputs($inputs, $county);

            return View::render(
                "county/county-form.twig.html",
                array("inputs" => $inputs, "county_id" => $id, "delete_link" => "county/delete/" . $id)
            );
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $county->fill($data);
                if (!Input::get("enabled")) {
                    $county->enabled = 0;
                } else {
                    $county->enabled = 1;
                }

                if ($id = $county->save()) {
                    $response = array("status" => 1, "message" => 'Županija je ažurirana');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }

    public function add()
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $county = new County();

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => "required",),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => '', "value" => "enabled"),
            );

            return View::render(
                "county/county-form.twig.html",
                array("inputs" => $inputs, "form_action" => "county/add",)
            );
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $county->fill($data);
                $county->enabled = (!Input::get("enabled") ? 0 : 1);

                if ($id = $county->save()) {
                    $response = array("status" => 1, "message" => 'Županija je dodana',  "action" => 'externalCrudFormDisable();');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }



    public function delete($id)
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $county = new County();
        if ($county->delete($id)) {
            echo "1";
        } else {
            echo "0";
        }
    }


} 