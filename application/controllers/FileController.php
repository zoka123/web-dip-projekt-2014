<?php

class FileController extends Controller
{
    public function violationFile()
    {
        if (!Input::get("form_timestamp")) {

            return View::render(
                "file/file-upload.twig.html",
                array("status" => "start")
            );

            echo "bok";
        } else {

            $allowedExts = array_merge(File::$imageTypes, File::$documentTypes);
            $temp = explode(".", $_FILES["file"]["name"]);
            $extension = end($temp);
            $filename = time() . "_" . rand(0, 100) . "_" . $_FILES["file"]["name"];

            if (in_array($extension, array_values($allowedExts))) {
                if ($_FILES["file"]["error"] > 0) {
                    echo "Greska: " . $_FILES["file"]["error"] . "<br>";
                } else {
                    if (move_uploaded_file(
                        $_FILES["file"]["tmp_name"],
                        "uploads/" . $filename
                    )
                    ) {
                        return View::render(
                            "file/file-upload.twig.html",
                            array("status" => "finished", "path" => "uploads/" . $filename, "filename" => $filename)
                        );
                    } else {
                        exit("Greska prilikom premjestanja datoteke");
                    }
                }
            } else {
                exit("Pogresan tip datoteke");
            }

        }
    }


} 