<?php

class ViolationController extends Controller
{

    public function __construct()
    {
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }

        $user = Session::get("user");
        $this->menu = Menu::getMenuByUserRole($user["user_role_id"]);
    }

    public function details($id){
        $violation = new Violation();
        $violation -> load($id);
        $details = $violation->details();

        return View::render("skeleton/details.twig.html", array("data" => $details));
    }


    public function allCategories()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));

        $columns = array(
            "violation_category_id" => "#",
            "name" => "Naziv",
            "period_of_limitation" => "Vrijeme zastare",
            "enabled" => "Status",
        );

        $paginator = new Paginator();
        $paginator->setColumns($columns);
        $count = ViolationCategory::count();
        $paginator->setCount($count);
        $paginator->prepare();

        $categories = ViolationCategory::select(
            array_keys($columns),
            User::MODEL_MODE_ARRAY,
            $paginator->getSQLParams()
        );

        $pagination = $paginator->getPaginationLinks(Input::all());

        $icons = array(
            "fa-edit" => "violation-category/edit/",
        );

        if (array_key_exists("enabled", $columns)) {
            foreach ($categories as &$category) {
                if ($category["enabled"] == 1) {
                    $category["enabled"] = '<i class="fa fa-check green-bg"></i>';
                } else {
                    $category["enabled"] = '<i class="fa fa-times red-bg"></i>';
                }
            }
        }

        View::render(
            'skeleton/crud-table.twig.html',
            array(
                'menu' => $this->menu,
                'base' => Config::get("base_url") . "violation-categories",
                'headers' => $paginator->columns,
                'key' => "violation_category_id",
                'add_link' => "violation-category/add",
                'data' => $categories,
                'pagination' => $pagination,
                'icons' => $icons,
            )
        );

    }

    public function editCategory($id)
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $category = new ViolationCategory();
        $category->load($id);

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => "required",),
                array(
                    "label" => "Vrijeme zastare",
                    "type" => "number",
                    "name" => "period_of_limitation",
                    "additional" => ' min="0"'
                ),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
            );

            Form::processInputs($inputs, $category);

            return View::render(
                "violation/violation-category-form.twig.html",
                array(
                    "inputs" => $inputs,
                    "violation_category_id" => $id,
                    "delete_link" => "violation-category/delete/" . $id
                )
            );
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $category->fill($data);
                $category->enabled = (!Input::get("enabled") ? 0 : 1);
                $category->period_of_limitation = (!Input::get("period_of_limitation") ? null : Input::get(
                    "period_of_limitation"
                ));

                if ($id = $category->save()) {
                    $response = array("status" => 1, "message" => 'Kategorija je ažurirana');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


    public function addCategory()
    {

        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $category = new ViolationCategory();

        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "Naziv", "type" => "text", "name" => "name", "additional" => "required",),
                array(
                    "label" => "Vrijeme zastare",
                    "type" => "number",
                    "name" => "period_of_limitation",
                    "additional" => ' min="0"'
                ),
                array("label" => "Omogućen", "type" => "checkbox", "name" => "enabled", "additional" => ''),
            );

            return View::render(
                "violation/violation-category-form.twig.html",
                array("inputs" => $inputs, "form_action" => "violation-category/add",)
            );
        } else {
            $data = Input::all();

            $rules = array(
                "name" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $category->fill($data);
                $category->enabled = (!Input::get("enabled") ? 0 : 1);
                $category->period_of_limitation = (!Input::get("period_of_limitation") ? null : Input::get(
                    "period_of_limitation"
                ));

                if ($id = $category->save()) {
                    $response = array(
                        "status" => 1,
                        "message" => 'Kategorija je dodana',
                        "action" => 'externalCrudFormDisable();'
                    );
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


    public function deleteCategory($id)
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN));
        $category = new ViolationCategory();
        if ($category->delete($id)) {
            echo "1";
        } else {
            echo "0";
        }
    }

    public function all()
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN, User::MODERATOR));

        $columns = array(
            "violation_id" => "#",
            "violation_category_id" => "Kategorija",
            "violation_ts" => "Datum",
            "city_id" => "Grad",
            "reporter_id" => "Prijavio",
            "user_id" => "Počinitelj",
            "description" => "Opis",
        );

        $paginator = new Paginator();
        $paginator->timeFilterColumn = "violation_ts";

        $paginator->setColumns($columns);
        $count = Violation::count();
        $paginator->setCount($count);
        $paginator->prepare();

        $violations = Violation::select(array_keys($columns), User::MODEL_MODE_ARRAY, $paginator->getSQLParams());

        $pagination = $paginator->getPaginationLinks(Input::all());

        $icons = array(
            "fa-edit" => "violation/edit/",
        );

        $cities = City::keyValue("city_id", "name");
        $officers = PoliceOfficer::customKeyValue();
        $users = User::customKeyValue();
        $categories = ViolationCategory::keyValue("violation_category_id", "name");

        foreach ($violations as &$violation) {
            $violation["violation_ts"] =date("d.m.Y. H:i", strtotime($violation["violation_ts"]));
            $violation["city_id"] = $cities[$violation["city_id"]];
            $violation["user_id"] = $users[$violation["user_id"]];
            $violation["reporter_id"] = $officers[$violation["reporter_id"]];
            $violation["violation_category_id"] = $categories[$violation["violation_category_id"]];
        }
/*
        $filterOptions = array(
            array("key" => "active", "value" => "Aktivni"),
            array("old" => "active", "value" => "U zastari"),
        );
*/
        View::render(
            'skeleton/crud-table.twig.html',
            array(
                'menu' => $this->menu,
                'base' => Config::get("base_url") . "violations",
                'headers' => $paginator->columns,
                'key' => "violation_id",
                'add_link' => "violation/add",
                'show_time_filters' => 1,
                'data' => $violations,
                'pagination' => $pagination,
                'icons' => $icons,
            )
        );

    }

    public function edit($id)
    {
        self::checkPermissions(Session::get("user"), array(User::ADMIN, User::MODERATOR));
        $violation = new Violation();
        $violation->load($id);

        if (!Input::get("form_timestamp")) {

            $reporterId = $violation->reporter_id;

            $inputs = array(
                array("label" => "Kategorija", "type" => "select", "class" => 'chosen-select',  "name" => "violation_category_id", "additional" => "required", "options" => ViolationCategory::keyValue("violation_category_id", "name")),
                array("label" => "Datum", "type" => "text", "name" => "violation_ts", "class" => "datetimepicker", "additional" => "required",),
                array("label" => "Opis", "type" => "text", "name" => "description", "additional" => "required",),
                array("label" => "Počinitelj", "type" => "select", "class" => 'chosen-select',  "name" => "user_id", "additional" => "required", "options" => User::customKeyValue()),
            );
            $user = Session::get("user");

            if($user["user_role_id"] == User::MODERATOR){
                $reporterOptions = PoliceOfficer::customKeyValue(" po.police_officer_id = ". $reporterId);
            } elseif($user["user_role_id"] == User::ADMIN){
                $reporterOptions = PoliceOfficer::customKeyValue();
            }
            $inputs[] = array("label" => "Prekršaj prijavio", "type" => "select", "class" => 'chosen-select',  "name" => "reporter_id", "additional" => "required", "options" => $reporterOptions);


            if($user["user_role_id"] == User::MODERATOR){
                $currentCity = new City();
                $currentCity->load($violation->city_id);
                $cityOptions = City::customKeyValue(" c.county_id = " . PoliceOfficer::getCountyByUserId($user["user_id"]));
                $cityOptions[$currentCity->city_id] = $currentCity->name;

            } elseif($user["user_role_id"] == User::ADMIN){
                $cityOptions = City::KeyValue("city_id", "name");
            }
            $inputs[] = array("label" => "Grad", "type" => "select", "class" => 'chosen-select',  "name" => "city_id", "additional" => "required", "options" => $cityOptions);


            Form::processInputs($inputs, $violation);

            $files = $violation->getDocuments();
            $inputs[] = array("label" => "Datoteke", "type" => "file", "files" => $files);
            $images = $violation->getImages();
            $inputs[] = array("label" => "Galerija", "type" => "gallery", "images" => $images);

            return View::render(
                "violation/violation-form.twig.html",
                array(
                    "inputs" => $inputs,
                    "violation_id" => $id,
                    "delete_link" => "violation/delete/" . $id
                )
            );
        } else {
            $data = Input::all();
            $rules = Violation::$rules;

            $newFiles= Input::get("newFiles");
            if(!empty($newFiles)){
                $newFiles  = explode("###FILE###", $newFiles);
            }

            $deletedFiles= Input::get("deletedFiles");
            if(!empty($deletedFiles)){
                $deletedFiles  = explode("###FILE###", $deletedFiles);
            }

            if(isset($data["newFiles"])){
                unset($data["newFiles"]);
            }

            if(isset($data["deletedFiles"])){
                unset($data["deletedFiles"]);
            }


            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $violation->fill($data);
                if ($id = $violation->save()) {

                    if(!empty($newFiles)){
                        foreach($newFiles as $newFile){
                            $file = new File();
                            $file -> filename = $newFile;
                            $parts = explode(".",$newFile);
                            $extension = (count($parts) ? end($parts) : "-");
                            $file->extension = $extension;
                            $file->type = File::FILE_TYPE_EVIDENCE;
                            $fileId = $file->save();

                            $violationFile = new ViolationFile();
                            $violationFile -> violation_id = $violation->getKey();
                            $violationFile -> file_id = $fileId;
                            $violationFile -> save();
                        }
                    }

                    $response = array("status" => 1, "message" => 'Prekršaj je ažuriran');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


    public function add()
    {
        {
            self::checkPermissions(Session::get("user"), array(User::ADMIN, User::MODERATOR));
            $violation = new Violation();
            $user = Session::get("user");
            $policeOfficer = PoliceOfficer::findWhere(array("user_id" => $user["user_id"]));

            if(empty($policeOfficer)){
                echo "Niste moderator! Mora Vam biti pridružen policajac da biste mogli dodati prekršaj";
                exit;
            } else {
                $policeOfficer = $policeOfficer[0];
            }

            if (!Input::get("form_timestamp")) {

                $reporterId = $violation->reporter_id;

                $inputs = array(
                    array("label" => "Kategorija", "type" => "select", "class" => 'chosen-select',  "name" => "violation_category_id", "additional" => "required", "options" => ViolationCategory::keyValue("violation_category_id", "name")),
                    array("label" => "Datum", "type" => "text", "name" => "violation_ts", "class" => "datetimepicker", "additional" => "required",),
                    array("label" => "Opis", "type" => "text", "name" => "description", "additional" => "required",),
                    array("label" => "Počinitelj", "type" => "select", "class" => 'chosen-select',  "name" => "user_id", "additional" => "required", "options" => User::customKeyValue()),
                );


                if($user["user_role_id"] == User::ADMIN){
                    $reporterOptions = PoliceOfficer::customKeyValue();
                    $inputs[] = array("label" => "Prekršaj prijavio", "type" => "select", "class" => 'chosen-select',  "name" => "reporter_id", "additional" => "required", "options" => $reporterOptions);
                }

                if($user["user_role_id"] == User::MODERATOR){
                    $currentCity = new City();
                    $currentCity->load($violation->city_id);
                    $cityOptions = City::customKeyValue(" c.county_id = " . PoliceOfficer::getCountyByUserId($user["user_id"]));
                    $cityOptions[$currentCity->city_id] = $currentCity->name;

                } elseif($user["user_role_id"] == User::ADMIN){
                    $cityOptions = City::KeyValue("city_id", "name");
                }
                $inputs[] = array("label" => "Grad", "type" => "select", "class" => 'chosen-select',  "name" => "city_id", "additional" => "required", "options" => $cityOptions);
                $inputs[] = array("label" => "Datoteke", "type" => "file", "files" => array());

                return View::render(
                    "violation/violation-form.twig.html",
                    array(
                        "inputs" => $inputs,
                        "form_action" => "violation/add",
                    )
                );
            } else {
                $data = Input::all();

                $newFiles= Input::get("newFiles");
                if(!empty($newFiles)){
                    $newFiles  = explode("###FILE###", $newFiles);
                }

                $deletedFiles= Input::get("deletedFiles");
                if(!empty($deletedFiles)){
                    $deletedFiles  = explode("###FILE###", $deletedFiles);
                }

                if(isset($data["newFiles"])){
                    unset($data["newFiles"]);
                }

                if(isset($data["deletedFiles"])){
                    unset($data["deletedFiles"]);
                }

                if($user["user_role_id"] == User::MODERATOR){
                    $data["reporter_id"] = $policeOfficer["police_officer_id"];
                }

                $rules = Violation::$rules;

                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    $response = array("status" => 0, "msg" => $validator->errors());
                    echo json_encode($response);
                } else {

                    $violation->fill($data);

                    if ($id = $violation->save()) {

                        if(!empty($newFiles)){
                            foreach($newFiles as $newFile){
                                $file = new File();
                                $file -> filename = $newFile;
                                $parts = explode(".",$newFile);
                                $extension = (count($parts) ? end($parts) : "-");
                                $file->extension = $extension;
                                $file->type = File::FILE_TYPE_EVIDENCE;
                                $fileId = $file->save();

                                $violationFile = new ViolationFile();
                                $violationFile -> violation_id = $id;
                                $violationFile -> file_id = $fileId;
                                $violationFile -> save();
                            }
                        }

                        $response = array("status" => 1, "message" => 'Prekršaj je dodan', "action" => 'externalCrudFormDisable();');
                        echo json_encode($response);
                    } else {
                        $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                        echo json_encode($response);
                    }
                }
            }
        }
    }


    public function delete($id)
    {
        $user = Session::get("user");

        $policeOfficer = PoliceOfficer::findWhere(array("user_id" => $user["user_id"]));

        if(empty($policeOfficer) && $user["user_role_id"] == User::MODERATOR){
            header('Content-Type: application/json');
            echo json_encode(array("msg" => "Niste moderator! Mora Vam biti pridružen policajac da biste mogli ažurirati prekršaj"));
            exit;
        } else {
            $policeOfficer = $policeOfficer[0];
        }


        self::checkPermissions($user, array(User::ADMIN, User::MODERATOR));
        $violation = new Violation();
        $violation -> load($id);

        if($user["user_role_id"] == User::MODERATOR && $violation->reporter_id != $policeOfficer["police_officer_id"]){
            header('Content-Type: application/json');
            echo json_encode(array("msg" => "Možete brisati samo svoje prekršaje"));
            exit;
        }

        if ($violation->delete($id)) {
            echo "1";
        } else {
            echo "0";
        }
    }


}