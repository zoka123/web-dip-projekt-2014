<?php

class ComplaintController extends Controller
{

    public function __construct()
    {
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }

        $user = Session::get("user");
        $this->menu = Menu::getMenuByUserRole($user["user_role_id"]);
    }

    public function details($id)
    {
        $complaint = new Complaint();
        $complaint->load($id);
        $details = $complaint->details();

        $files = $complaint->getDocuments();
        $images = $complaint->getImages();

        return View::render("skeleton/details.twig.html", array("data" => $details, "files" => $files, "images" => $images));
    }

    public function reject($id){
        $complaint = new Complaint();
        $complaint->load($id);
        $complaint->complaint_status_id = 3;
        if($complaint->save()){
            exit('<script>alert("Žalba odbijena"); window.location.reload()</script>');
        }
    }

    public function accept($id){
        $complaint = new Complaint();
        $complaint->load($id);
        $complaint->complaint_status_id = 2;
        if($complaint->save()){
            exit('<script>alert("Žalba prihvaćena"); window.location.reload()</script>');
        }
    }


    public function all()
    {
        $sessionUser = Session::get("user");
        self::checkPermissions($sessionUser, array(User::ADMIN, User::REGISTERED, User::MODERATOR));

        $columns = array(
            "complaint_id" => "#",
            "complaint_status_id" => "STATUS",
            "text" => "Tekst",
        );

        $paginator = new Paginator();

        $paginator->setColumns($columns);
        $count = Complaint::count(array("where" => " AND u.user_id = " . $sessionUser["user_id"]));

        $paginator->setCount($count);
        $paginator->prepare();


        $params = $paginator->getSQLParams();

        if ($sessionUser["user_role_id"] == User::REGISTERED) {
        }

        $complaints = Complaint::select(array_keys($columns), User::MODEL_MODE_ARRAY, $params);

        $pagination = $paginator->getPaginationLinks(Input::all());

        $icons = array(
            "fa-search" => "complaint/view/",
        );

        foreach ($complaints as &$complaint) {
            $complaint["complaint_status_id"] = Complaint::$COMPLAINT_STATUS[$complaint["complaint_status_id"]];
        }

        $data = array(
            'menu' => $this->menu,
            'base' => Config::get("base_url") . "complaints",
            'headers' => $paginator->columns,
            'key' => "complaint_id",
            'data' => $complaints,
            'hideSearch' => true,
            'pagination' => $pagination,
            'icons' => $icons,
        );

        if ($sessionUser["user_role_id"] == User::REGISTERED) {
            $data["add_link"] = "complaint/add";
        }


        View::render(
            'skeleton/crud-table.twig.html',
            $data
        );

    }

    public function add($violationId)
    {

        $complaint = Complaint::findWhere(array("violation_id" => $violationId));
        if(!empty($complaint)){
            exit("Žalba je već predana");
        }

        self::checkPermissions(Session::get("user"), array(User::REGISTERED));
        $complaint = new Complaint();
        $complaint->violation_id = $violationId;
        $complaint->complaint_status_id = 1;
        $user = Session::get("user");

        $violation = Violation::findWhere(array("violation_id" => $violationId));

        if (!Input::get("form_timestamp")) {

            $inputs = array(
                array(
                    "label" => "Tekst",
                    "type" => "text",
                    "name" => "text",
                    "additional" => "required",
                ),
            );

            $inputs[] = array("label" => "Datoteke", "type" => "file", "files" => array());

            return View::render(
                "complaint/complaint-form.twig.html",
                array(
                    "inputs" => $inputs,
                    "form_action" => "complaint/add/".$violationId,
                )
            );
        } else {
            $data = Input::all();

            $newFiles = Input::get("newFiles");
            if (!empty($newFiles)) {
                $newFiles = explode("###FILE###", $newFiles);
            }

            $deletedFiles = Input::get("deletedFiles");
            if (!empty($deletedFiles)) {
                $deletedFiles = explode("###FILE###", $deletedFiles);
            }

            if (isset($data["newFiles"])) {
                unset($data["newFiles"]);
            }

            if (isset($data["deletedFiles"])) {
                unset($data["deletedFiles"]);
            }

            $rules = Complaint::$rules;

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $complaint->fill($data);

                if ($id = $complaint->save()) {

                    if (!empty($newFiles)) {
                        foreach ($newFiles as $newFile) {
                            $file = new File();
                            $file->filename = $newFile;
                            $parts = explode(".", $newFile);
                            $extension = (count($parts) ? end($parts) : "-");
                            $file->extension = $extension;
                            $file->type = File::FILE_TYPE_EVIDENCE;
                            $fileId = $file->save();

                            $complaintFile = new ComplaintFile();
                            $complaintFile->complaint_id = $id;
                            $complaintFile->file_id = $fileId;
                            $complaintFile->save();
                        }
                    }

                    $response = array(
                        "status" => 1,
                        "message" => 'Žalba je dodana',
                    );
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }


}