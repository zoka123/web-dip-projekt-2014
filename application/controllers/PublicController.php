<?php

class PublicController extends Controller
{
    public static $menuItems = array(
        "PublicController@index" => "Početna",
        "PublicController@login" => "Prijava",
        "PublicController@register" => "Registracija",
    );

    public function __construct()
    {
        $this->menu = "public-menu";
    }

    public function stationsByCounty($countyId)
    {
        $stations = PoliceStation::findWhere(array("county_id" => $countyId, "enabled" => 1));
        echo json_encode($stations);
    }

    public function officersByStation($stationId)
    {
        $station = new PoliceStation();
        $station->load($stationId);
        $officers = $station->enabledOfficers();

        echo json_encode($officers);
    }

    public function officerDetails($officerId)
    {
        $officer = new PoliceOfficer();
        $officer->load($officerId);
        $details = $officer->details();

        return View::render("officer/details.twig.html", array("officer" => $details));
    }

    public function index()
    {
        if (Auth::check()) {
            Redirect::route("admin");
        }

        $counties = County::all();

        $params = array(
            "menu" => $this->menu,
            "counties" => $counties,
        );

        return View::render("index.twig.html", $params);
    }


    public function register()
    {
        if (!Input::get("form_timestamp")) {
            $inputs = array(
                array("label" => "OIB", "type" => "text", "name" => "oib", "additional" => ' class="check-free" required ',),
                array("label" => "Ime", "type" => "text", "name" => "first_name", "additional" => "required",),
                array("label" => "Prezime", "type" => "text", "name" => "last_name", "additional" => "required",),
                array("label" => "Email", "type" => "email", "name" => "email", "additional" => ' class="check-free" required ',),
                array("label" => "Lozinka", "type" => "password", "name" => "password", "additional" => ' pattern=".{6,}" ', "validationMessage" => "Minimalno 6 znakova!"),
                array("label" => "Datum rođenja", "type" => "text", "name" => "birthdate", "additional" => 'required class="datepicker"'),
                array("label" => "Adresa", "type" => "text", "name" => "address", "additional" => 'required'),
                array("label" => "Opis", "type" => "text", "name" => "description", "additional" => ''),
                array("label" => "Telefon", "type" => "phone", "name" => "telephone", "additional" => ''),
            );

            return View::render("register/register.twig.html", array("inputs" => $inputs));
        } else {
            $data = Input::all();

            $validator = Validator::make($data, User::$VALIDATION_RULES);

            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {

                $checkOib = User::findWhere(array("oib" => $data["oib"]), " deleted_at is NULL");
                $checkEmail = User::findWhere(array("email" => $data["email"]), " deleted_at is NULL");

                if((int)(count($checkOib) != 0)){
                    $response = array("status" => 0, "msg" => array("oib" => array("Vrijednost polja # je zauzeta")));
                    exit(json_encode($response));
                }
                if((int)(count($checkEmail) != 0)){
                    $response = array("status" => 0, "msg" => array("email" => array("Vrijednost polja # je zauzeta")));
                    exit(json_encode($response));
                }

                $user = new User();
                $user -> fill($data);
                $user->user_role_id = User::DEFAULT_USER_ROLE;
                $user->enabled = "false";

                if ($id = $user->save()) {
                    $link = Config::get("base_url") . "user/activate/".$id."/".md5($id . "activation");
                    $mail = 'Vaš korisnički račun je kreiran.<br/>Molimo posjetite <a href="'.$link.'">aktivacijski link</a> kako biste aktivirali svoj korisnički račun unutar 24 sata';
                    Email::compose($mail);
                    if(Email::send($user->email, "projekt@webdip.foi.hr", "Aktivacijski link")){
                        $response = array("status" => 1, "message" => 'Registracija je zaprimljena i aktivacijski link je poslan na email');
                        echo json_encode($response);
                    }
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Došlo je do greške"));
                    echo json_encode($response);
                }
            }
        }
    }

    public function check($field, $value = null){
        if($value == null){
            $value = Input::get($field);
        }

        $result = User::findWhere(array($field => $value), " deleted_at is NULL");

        if(count($result) == 0){
            echo 1;
        } else {
            echo 0;
        }
        return;
    }

    public function login()
    {
        if (!Input::get("form_timestamp")) {

            $email = Cookie::get("last_email");

            $inputs = array(
                array("label" => "Email", "type" => "email", "name" => "email", "additional" => "required", "value" => $email),
                array("label" => "Lozinka", "type" => "password", "name" => "password", "additional" => "required"),
            );

            return View::render("login/login.twig.html", array("inputs" => $inputs));
        } else {
            $data = Input::all();

            $rules = array(
                "email" => "required",
                "password" => "required",
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = array("status" => 0, "msg" => $validator->errors());
                echo json_encode($response);
            } else {
                if (Auth::attempt($data)) {
                    $response = array("status" => 1, "action" => 'window.location = "'.Config::get("base_url").'admin"');
                    echo json_encode($response);
                } else {
                    $response = array("status" => 0, "msg" => array("general" => "Korisnik ne postoji"));
                    echo json_encode($response);
                }
            }
        }
    }

    public
    function logout()
    {
        Auth::logout();
        Redirect::route("");
    }


    public function activate($id, $check)
    {

        if (md5($id . "activation") === $check) {

            $user = new User();
            $user->load($id);

            $userCreatedAt = strtotime($user->created_at);

            if(($userCreatedAt + 24 * 3600) < VirtualTime::vtime()){
                throw new Exception("Vrijeme za aktivaciju je isteklo");
            };

            if($user->attempts >= 3 || !empty($user->last_login_at)){
                throw new Exception("Nije moguće aktivirati račun. Jeste li već aktivirali svoj račun koristeći ovaj link?");
            }

            if($user->enabled != 0){
                throw new Exception("Nije moguće aktivirati račun.");
            }

            $user->enabled = 1;
            if($user->save()){
                return View::render("skeleton/message.twig.html", array("label" => "Uspjeh!", "message" => "Korisnički račun je uspješno aktiviran!", "menu" => "public-menu"));
            } else {
                return View::render("skeleton/message.twig.html", array("label" => "Greška!", "message" => "Došlo je do pogreške!", "menu" => "public-menu"));
            }

        } else {
            throw new Exception("Pogrešni pristupni podaci");
        }

    }

    public function reset(){
        $email = Input::get("email");
        if(empty($email)){
            exit("0");
        } else {
            $user = User::findWhere(array("email" => $email));
            if(!count($user)){
                exit("0");
            } else {
                $password = substr(md5(time()).rand(),0,10);
                $mail = 'Vaša je lozinka promijenjena<br/>Nova lozinka je '.$password;
                $userInstance = new User();
                $userInstance -> load($user[0]["user_id"]);
                $userInstance -> password = $password;
                if($userInstance -> save()){
                    Email::compose($mail);
                    if(Email::send($user[0]["email"], "projekt@webdip.foi.hr", "Nova lozinka")){
                        exit("1");
                    }
                }
            }
        }
        exit("0");
    }

} 