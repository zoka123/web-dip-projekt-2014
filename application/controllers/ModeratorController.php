<?php

class ModeratorController extends Controller
{

    public function __construct()
    {
        $user = Session::get("user");
        if (!Auth::check()) {
            throw new LoginRequiredException();
        }
        self::checkPermissions($user, array(User::MODERATOR, User::ADMIN));

        $this->menu = "moderator-menu";
    }

    public static function arrayToJsArray($data, $labels)
    {

        $ret = "";
        $ret .= '[\'' . $labels[0] . '\', \'' . $labels[1] . '\'],';

        foreach ($data as $k => &$d) {
            $d = array_values($d);
            $ret .= '[\'' . $d[0] . '\', ' . $d[1] . '],';
        }

        $ret = substr($ret, 0, -1);

        return $ret;
    }

    public function index()
    {
        $sessionUser = Session::get("user");
        $user = new User();
        $user->load($sessionUser["user_id"]);

        $officerFinder = PoliceOfficer::findWhere(array("user_id" => $sessionUser["user_id"]));
        $officer = new PoliceOfficer();
        $officer->load($officerFinder[0]["police_officer_id"]);

        $complaintsByCounty = Complaint::complaintsByCounty();
        $complaintsByCountyJson = self::arrayToJsArray($complaintsByCounty, array("Županija", "Broj žalbi"));

        $complaintsByUser = Complaint::complaintsByUser();

        $violationsByCounty = Violation::violationsByCounty();
        $violationsByCountyJson = self::arrayToJsArray($violationsByCounty, array("Županija", "Broj prekršaja"));


        $violationsByUser = Violation::violationsByUser();

        $complaints = $officer->complaints();
//        dd($complaints);

        View::render(
            'moderator/dashboard.twig.html',
            array(
                "menu" => $this->menu,
                "complaints" => array(
                    "data" => $complaints,
                    "base" => Config::get("base_url") . "api/complaint-details/"
                ),
                "complaintsByCounty" => array("data" => $complaintsByCounty),
                "complaintsByCountyJson" => $complaintsByCountyJson,
                "complaintsByUser" => array("data" => $complaintsByUser),
                "violationsByCounty" => array("data" => $violationsByCounty),
                "violationsByUser" => array("data" => $violationsByUser),
                "violationsByCountyJson" => $violationsByCountyJson,
            )
        );
    }

    public function countyStatsPDF()
    {
        define('DOMPDF_ENABLE_AUTOLOAD', false);
        define('DOMPDF_ENABLE_REMOTE', true);
        require_once 'vendor/dompdf/dompdf/dompdf_config.inc.php';

        //http://chart.googleapis.com/chart?cht=p&chs=500x300&chl=May|June|July|August|September|October&chd=t:1,2,3,4,5,6,7,8
        $url = 'http://chart.googleapis.com/chart?cht=p&chs=700x300&';

        $keys = array();
        $values = array();

        $complaintsByCounty = Complaint::complaintsByCounty();
        foreach ($complaintsByCounty as $k => $v) {
            $keys[] = urlencode($v["name"]);
            $values[] = urlencode($v["count"]);
        }
        $keys = implode("|", $keys);
        $values = implode(",", $values);
        $url = $url . ("chdl=" . $keys . "&chl=" . $keys . "&chd=t:" . $values);
        $png = file_get_contents($url);
        $path1 = 'cache/chart1.png';
        file_put_contents($path1, $png);

        $url2 = 'http://chart.googleapis.com/chart?chco=0000FF&cht=p&chs=700x300&';
        $violationsPerCounty = Violation::violationsByCounty();
        foreach ($violationsPerCounty as $k => $v) {
            $keys2[] = urlencode($v["name"]);
            $values2[] = urlencode($v["count"]);
        }
        $keys2 = implode("|", $keys2);
        $values2 = implode(",", $values2);
        $url2 = $url2 . ("chdl=" . $keys2 . "&chl=" . $keys2 . "&chd=t:" . $values2);
        $png2 = file_get_contents($url2);
        $path2 = 'cache/chart2.png';
        file_put_contents($path2, $png2);



        $img1 = '<img src="' . $path1 . '"/>';
        $img2 = '<img src="' . $path2 . '"/>';

        $document = '<html>
 <body>
  <h1>Žalbe po županijama</h1>
  '.$img1.'
  <hr>
  <h1>Prekršaji po županijama</h1>
  '.$img2.'
 </body>
</html>';

        $pdf = new DOMPDF();
        $pdf->load_html($document);
        $pdf->render();
        $pdf->stream("stats.pdf");

    }
}