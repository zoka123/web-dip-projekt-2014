<?php

class County extends Model
{
    public static $attributes = array();
    protected static $table = "county";
    protected static $primaryKey = "county_id";

    public static $editable = array(
        "name",
        "enabled",
    );

}