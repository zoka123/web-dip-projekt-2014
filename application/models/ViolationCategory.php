<?php

class ViolationCategory extends Model
{
    const PERIOD_TWO_YEARS = 730;
    const PERIOD_ONE_YEAR = 365;
    const PERIOD_ONE_MONTH = 30;
    const PERIOD_SIX_MONTH = 180;
    const PERIOD_ONE_WEEK = 7;

    public static $attributes = array();
    protected static $table = "violation_category";
    protected static $primaryKey = "violation_category_id";

    public static $editable = array(
        "name",
        "period_of_limitation",
        "enabled",
    );

}