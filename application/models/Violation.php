<?php

class Violation extends Model
{

    public static $timeFilterColumn = "violation_ts";

    public static $attributes = array();
    protected static $table = "violation";
    protected static $primaryKey = "violation_id";

    public static $editable = array(
        "violation_category_id",
        "reporter_id",
        "violation_ts",
        "user_id",
        "description",
        "city_id",
    );

    public static $rules = array(
        "violation_category_id" => "required",
        "reporter_id" => "required",
        "violation_ts" => "required",
        "description" => "required",
        "city_id" => "required",
        "city_id" => "required",
    );

    public function get_violation_ts()
    {
        $date = $this->properties["violation_ts"]->value;

        return date("d.m.Y. H:i", strtotime($date));
    }

    public static function getEndingToday()
    {
        $datetime = datetime(VirtualTime::vtime());

        $sql = <<<SQL
        SELECT v.violation_ts as created, (v.violation_ts + INTERVAL vc.period_of_limitation DAY) as ending, v.violation_id, CONCAT(u.first_name, " ", u.last_name) as user, u.user_id, vc.name as category FROM user u
join violation v on v.user_id = u.user_id
join violation_category vc on vc.violation_category_id = v.violation_category_id
WHERE (v.violation_ts + INTERVAL vc.period_of_limitation DAY) >= "#NOW#" AND (v.violation_ts + INTERVAL vc.period_of_limitation DAY) <= "#NOW#" + INTERVAL 1 DAY ORDER BY ending
SQL;
        $sql = str_replace("#NOW#", $datetime, $sql);

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public static function getEndingFuture()
    {
        $datetime = datetime(VirtualTime::vtime());

        $sql = <<<SQL
        SELECT v.violation_ts as created, (v.violation_ts + INTERVAL vc.period_of_limitation DAY) as ending, v.violation_id, CONCAT(u.first_name, " ", u.last_name) as user, u.user_id, vc.name as category FROM user u
join violation v on v.user_id = u.user_id
join violation_category vc on vc.violation_category_id = v.violation_category_id
WHERE (v.violation_ts + INTERVAL vc.period_of_limitation DAY) > "#NOW#" + INTERVAL 1 DAY ORDER BY ending LIMIT 25
SQL;
        $sql = str_replace("#NOW#", $datetime, $sql);

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public static function allActive($params = array())
    {
        $datetime = datetime(VirtualTime::vtime());

        $sql = <<<SQL
        SELECT v.violation_ts as created, (v.violation_ts + INTERVAL vc.period_of_limitation DAY) as ending, v.violation_id, CONCAT(u.first_name, " ", u.last_name) as user, u.user_id, vc.name as category FROM user u
join violation v on v.user_id = u.user_id
join violation_category vc on vc.violation_category_id = v.violation_category_id
WHERE (v.violation_ts + INTERVAL vc.period_of_limitation DAY) > "#NOW#" + INTERVAL 1 DAY
SQL;

        if (isset($params["limit"])) {
            $sql .= $params["limit"];
        }

        if (isset($params["order"])) {
            $sql .= $params["order"];
        }

        $sql = str_replace("#NOW#", $datetime, $sql);

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }


    public function getFiles($where = "")
    {

        $sql = <<<SQL
SELECT * FROM violation_file vf join file f on vf.file_id = f.file_id WHERE vf.violation_id =
SQL;
        $sql .= $this->getKey();
        $sql .= $where;

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;

    }

    public function getDocuments()
    {
        $types = implode("\",\"", File::$documentTypes);
        $where = " AND extension IN (\"" . $types . "\")";

        return $this->getFiles($where);
    }

    public function getImages()
    {
        $types = implode("\",\"", File::$imageTypes);
        $where = " AND extension IN (\"" . $types . "\")";

        return $this->getFiles($where);
    }

    public function details()
    {
        $sql = <<<SQL
        SELECT
        v.violation_ts as created,
        c.name as city,
        IF( vc.period_of_limitation IS NOT NULL, (v.violation_ts + INTERVAL vc.period_of_limitation DAY), "NIKADA") as ending,
        CONCAT(u.first_name, " ", u.last_name) as user,
        vc.name as category

        FROM user u
join violation v on v.user_id = u.user_id
join city c on v.city_id = c.city_id
join violation_category vc on vc.violation_category_id = v.violation_category_id
WHERE v.violation_id =
SQL;

        $sql .= $this->getKey();
        $stmt = DB::dbh()->query($sql);

        $results = $stmt->fetch(PDO::FETCH_ASSOC);

        $return = array();

        $labels = array(
            "created" => "Datum",
            "city" => "Grad",
            "ending" => "Zastara",
            "user" => "Počinitelj",
            "category" => "Kategorija",
        );

        if (count($results)) {
            foreach ($results as $k => $result) {
                if(in_array($k, array("created", "ending")) && $result != "NIKADA"){
                    $result = date("d.m.y H:i", strtotime($result));
                }
            $return[] = array("label" => $labels[$k], "value" => $result);
            }
        }
        return $return;
    }

    public static function violationsByCounty(){
        $sql = 'Select co.name, count(*) as count from violation v  join city ci on ci.city_id = v.city_id join county co on ci.county_id = co.county_id group by co.name';
        $stmt = DB::dbh()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public static function violationsByUser(){
        $sql = 'Select CONCAT(u.first_name," ", u.last_name) as name, count(*) as count from violation v join user u on v.user_id = u.user_id group by name';
        $stmt = DB::dbh()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

}