<?php

class ComplaintFile extends Model
{
    public static $attributes = array();
    protected static $table = "complaint_file";
    protected static $primaryKey = "complaint_file_id";

    public static $editable = array(
        "complaint_id",
        "file_id",
    );

}