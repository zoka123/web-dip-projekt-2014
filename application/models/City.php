<?php

class City extends Model
{
    public static $attributes = array();
    protected static $table = "city";
    protected static $primaryKey = "city_id";

    public static $editable = array(
        "name",
        "county_id",
        "city_zip_code",
        "enabled",
    );

    public static function customKeyValue($where = null){

        $sql = <<<SQL
        SELECT
        c.city_id as `key`, c.name as `value`
        from city c
        WHERE c.deleted_at is NULL
SQL;

        if($where != null){
            $sql .= " AND " . $where;
        }

        $stmt = DB::dbh()->query($sql);


        $results =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $return = array();

        foreach($results as $result){
            $return[$result["key"]] = $result["value"];
        }
        return $return;
    }


}