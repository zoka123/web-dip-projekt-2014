<?php

class PoliceOfficer extends Model
{
    public static $attributes = array();
    protected static $table = "police_officer";
    protected static $primaryKey = "police_officer_id";

    private $user = null;

    public static $editable = array(
        "user_id",
        "licence_number",
        "police_station_id",
        "licence_expiration_date",
        "licence_expiration_date",
    );

    private function getUser()
    {
        if ($this->user == null) {
            $this->user = new User;
            $this->user->load($this->user_id);
        }

        return $this->user;
    }


    public function details()
    {
        $details = array(
            array("label" => "Ime", "value" => $this->getUser()->first_name),
            array("label" => "Prezime", "value" => $this->getUser()->last_name),
            array("label" => "Broj dozvole", "value" => $this->licence_number),
            array("label" => "Datum isteka dozvole", "value" => $this->licence_expiration_date),
        );

        return $details;
    }

    public static function customKeyValue($where = null)
    {

        $sql = <<<SQL
        SELECT
        po.police_officer_id as `key`,
        CONCAT(u.first_name, " ", u.last_name) as `value`
        from police_officer po join user u on po.user_id = u.user_id
        WHERE po.deleted_at is NULL and u.deleted_at is NULL
SQL;

        if ($where != null) {
            $sql .= " AND " . $where;
        }

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $return = array();

        foreach ($results as $result) {
            $return[$result["key"]] = $result["value"];
        }

        return $return;
    }

    public static function getCountyByUserId($userId){
        $sql = <<<SQL
        SELECT ps.county_id
        FROM police_officer po
        JOIN police_station ps ON po.police_station_id = ps.police_station_id
        WHERE po.user_id =
SQL;

        $sql .= $userId;

        $stmt = DB::dbh()->query($sql);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result["county_id"];

    }


    public function complaints()
    {
        $sql = <<<SQL
        SELECT v.violation_id, c.complaint_id, IF( c.complaint_status_id =1,  "NA ČEKANJU", IF( c.complaint_status_id =2,  "PRIHVACENA",  "ODBIJENA" ) ) AS
STATUS , vc.name AS category
FROM user u
JOIN violation v ON v.user_id = u.user_id
JOIN violation_category vc ON vc.violation_category_id = v.violation_category_id
JOIN complaint c ON c.violation_id = v.violation_id
WHERE v.reporter_id =
SQL;

        $sql .= $this->getKey();
        $stmt = DB::dbh()->query($sql);

        $complaints = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $return = array();

        $labels = array(
            "complaint_id" => "#",
            "violation_id" => "Prekršaj",
            "STATUS" => "Status",
            "category" => "Kategorija",
        );

        if (count($complaints)) {
            foreach ($complaints as $result) {
                $row = array();
                foreach ($result as $k => $value) {
                    if (in_array($k, array("created", "ending")) && $value != "NIKADA") {
                        $value = date("d.m.y H:i", strtotime($value));
                    }
                    $row[] = array("label" => $labels[$k], "value" => $value);
                }
                $return[$result["complaint_id"]] = $row;
            }
        }
        return $return;
    }


}