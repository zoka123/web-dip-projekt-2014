<?php

class PoliceStation extends Model
{
    public static $attributes = array();
    protected static $table = "police_station";
    protected static $primaryKey = "police_station_id";

    public static $editable = array(
        "county_id",
        "name",
        "address",
        "telephone",
        "enabled",
    );

    public function officers(){
        $sql = <<<SQL
        SELECT u.*, po.* from
user u join police_officer po on u.user_id = po.user_id
WHERE  po.police_station_id =
SQL;
        $sql .= $this->getKey();

        $stmt = DB::dbh()->query($sql);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function enabledOfficers(){
        $sql = <<<SQL
        SELECT u.*, po.* from
user u join police_officer po on u.user_id = po.user_id
WHERE  u.enabled = 1 AND po.enabled = 1 AND po.police_station_id =
SQL;
        $sql .= $this->getKey();

        $stmt = DB::dbh()->query($sql);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function keyValueOfficers(){
        $officers = $this->officers();

        $return = array();

        foreach($officers as $result){
            $return[$result["police_officer_id"]] = $result["first_name"] . " " . $result["last_name"];
        }
        return $return;
    }

    public function clearOfficers($except = array()){
        $exceptString = "";
        if(count($except)){
            $exceptString = implode(",",$except);
        }

        $sql = ' UPDATE police_officer set police_station_id = null WHERE police_station_id = ' . $this -> getKey();

        if(!empty($exceptString)){
            $sql .= ' AND police_officer_id not in (' . $exceptString . ")";
        }
        $stmt = DB::dbh()->prepare($sql);
        return $stmt -> execute();
    }

}