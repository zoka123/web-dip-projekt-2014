<?php

class Log extends Model
{

    public static $LOG_TYPES = array(
        "LOG_TYPE_GENERAL" => 0,
        "LOG_TYPE_BAD_LOGIN" => 1,
        "LOG_TYPE_SUCCESSFUL_LOGIN" => 2,
        "DATABASE_OPERATION" => 3,
        "LOG_TYPE_LOGOUT" => 4,
    );

    public static $attributes = array();
    public static $table = "log";
    protected static $primaryKey = "log_id";

    public static $rules = array(
        "log_type" => "required",
    );

    public static $editable = array(
        "log_type",
        "user_id",
        "query",
        "additional",
    );

    public static function add($type, $additional = null, $query = null, $userId = null)
    {
        if ($userId === null) {
            try {
                if ($user = Session::get("user")) {
                    $userId = $user["user_id"];
                }
            } catch (Exception $e) {

            }
        }

        $log = new Log();
        if (!in_array($type, self::$LOG_TYPES)) {
            $type = self::$LOG_TYPES["LOG_TYPE_GENERAL"];
        }

        $log->log_type = $type;
        $log->user_id = $userId;
        $log->query = $query;
        $log->additional = $additional;

        $log->save();

    }

    public static function getLogType($typeId)
    {
        if (!in_array($typeId, array_values(self::$LOG_TYPES))) {
            return "LOG_TYPE_GENERAL";
        } else {
            foreach(self::$LOG_TYPES as $k => $logType){
                if($logType == $typeId){
                    return $k;
                }
            }
        }
    }

}