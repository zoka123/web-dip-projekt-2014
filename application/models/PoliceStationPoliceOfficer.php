<?php

class PoliceStationPoliceOfficer extends Model
{
    public static $attributes = array();
    protected static $table = "police_station_has_police_officer";
    protected static $primaryKey = "id";

    public static $editable = array(
        "police_officer_id",
        "police_station_id",
        "active_from",
        "active_to",
    );

}