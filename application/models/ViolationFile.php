<?php

class ViolationFile extends Model
{
    public static $attributes = array();
    protected static $table = "violation_file";
    protected static $primaryKey = "violation_file_id";

    public static $editable = array(
        "violation_id",
        "file_id",
    );

}