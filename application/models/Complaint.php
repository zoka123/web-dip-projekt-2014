<?php

class Complaint extends Model
{

    public static $COMPLAINT_STATUS = array(
      1 => "U OBRADI",
      2 => "PRIHVAĆENA",
      3 => "ODBIJENA",
    );

    public static $attributes = array();
    protected static $table = "complaint";
    protected static $primaryKey = "complaint_id";

    public static $editable = array(
        "violation_id",
        "complaint_status_id",
        "text",
    );

    public static $rules = array(
        "text" => "required",
    );


    public function getFiles($where = "")
    {

        $sql = <<<SQL
SELECT * FROM complaint_file cf join file f on cf.file_id = f.file_id WHERE cf.complaint_id =
SQL;
        $sql .= $this->getKey();
        $sql .= $where;

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;

    }

    public function getDocuments()
    {
        $types = implode("\",\"", File::$documentTypes);
        $where = " AND extension IN (\"" . $types . "\")";

        return $this->getFiles($where);
    }

    public function getImages()
    {
        $types = implode("\",\"", File::$imageTypes);
        $where = " AND extension IN (\"" . $types . "\")";

        return $this->getFiles($where);
    }

    public function details()
    {
        $sql = <<<SQL
        SELECT
        v.violation_id as violation,
        vc.name as category,
        IF( c.complaint_status_id = 1, "NA ČEKANJU", IF( c.complaint_status_id = 2, "PRIHVACENA", "ODBIJENA")) as status,
        c.text as text

        FROM violation_category vc
join violation v on vc.violation_category_id = v.violation_category_id
join complaint c on c.violation_id = v.violation_id
WHERE c.complaint_id =
SQL;

        $sql .= $this->getKey();
        $stmt = DB::dbh()->query($sql);

        $results = $stmt->fetch(PDO::FETCH_ASSOC);

        $return = array();

        $labels = array(
            "violation" => "Prekršaj",
            "status" => "Status",
            "text" => "Tekst",
            "category" => "Kategorija",
        );

        if (count($results)) {
            foreach ($results as $k => $result) {
                $return[] = array("label" => $labels[$k], "value" => $result);
            }
        }

        return $return;
    }

    public static function count($params = array(), $includeDeleted = false)
    {
        static::loadAttributes();
        $sql ='SELECT COUNT(*) as count from complaint c join violation v on c.violation_id = v.violation_id join user u on v.user_id = u.user_id WHERE c.deleted_at is null';

        if(isset($params["where"])){
            $sql .= $params["where"];
        }

        $stmt = DB::dbh()->prepare($sql);

        $stmt->execute();

        $results = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$results) {
            debug($stmt->getSQL());
        }

        return $results["count"];

    }


    public static function select($columns, $mode = self::MODEL_MODE_ARRAY, $params = null, $includeDeleted = false)
    {
        static::loadAttributes();

        $order = self::buildQuery(null, $params, $columns);
        $order = substr($order,strpos($order,"ORDER"));

        $sql ='SELECT c.complaint_id, c.complaint_status_id, c.text from complaint c join violation v on c.violation_id = v.violation_id join user u on v.user_id = u.user_id WHERE c.deleted_at is null ';

        if(isset($params["where"])){
            $sql .= $params["where"];
        }

        $sql  .= $order;

        $stmt = DB::dbh()->prepare($sql);

        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!$results) {
            debug($stmt->getSQL());
        }

        if ($mode === self::MODEL_MODE_OBJECT) {
            $return = array();
            $class = get_called_class();
            foreach ($results as $result) {
                $model = new $class;
                $model->setValues($result);
                $return[] = $model;
            }
            return $return;
        } else {
            return $results;
        }

    }

    public static function complaintsByCounty(){
        $sql = 'Select co.name, count(*) as count from complaint c join violation v on c.violation_id = v.violation_id join city ci on ci.city_id = v.city_id join county co on ci.county_id = co.county_id group by co.name';
        $stmt = DB::dbh()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public static function complaintsByUser(){
        $sql = 'Select CONCAT(u.first_name," ", u.last_name) as name, count(*) as count from complaint c join violation v on c.violation_id = v.violation_id join user u on v.user_id = u.user_id group by name';
        $stmt = DB::dbh()->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }


}