<?php

class User extends zf_User
{


    public static $attributes = array();

    public static $VALIDATION_RULES = array(
        "oib" => "required|numeric|min:11|max:11",
        "first_name" => "required",
        "last_name" => "required",
        "email" => "required|email",
        "password" => "required|min:6",
        "birthdate" => "required",
    );

    const REGISTERED = 3;
    const MODERATOR = 2;
    const ADMIN = 1;

    const DEFAULT_USER_ROLE = 3;

    protected static $primaryKey = "user_id";

    public function get_birthdate()
    {
        $date = $this->properties["birthdate"]->value;

        return date("d.m.Y.", strtotime($date));
    }

//    public function get_last_login_at()
//    {
//        $date = $this->properties["last_login_at"]->value;
//        return date("d.m.Y.", strtotime($date));
//    }

    public static function customKeyValue($where = null)
    {

        $sql = <<<SQL
        SELECT
        u.user_id as `key`,
        CONCAT(u.first_name, " ", u.last_name) as `value`
        FROM user u
        WHERE u.deleted_at is NULL
SQL;

        if ($where != null) {
            $sql .= " AND " . $where;
        }

        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $return = array();

        foreach ($results as $result) {
            $return[$result["key"]] = $result["value"];
        }

        return $return;
    }

    public function details()
    {
        $sql = <<<SQL
        SELECT CONCAT(first_name, " ", last_name) as name, email, address from user where user_id =
SQL;
        $sql .= $this->getKey();
        $stmt = DB::dbh()->query($sql);
        $results = $stmt->fetch(PDO::FETCH_ASSOC);
        $return = array();
        $labels = array(
            "name" => "Ime i prezime",
            "email" => "Email",
            "address" => "Adresa",
        );

        if (count($results)) {
            foreach ($results as $k => $result) {
                $return[] = array("label" => $labels[$k], "value" => $result);
            }
        }

        return $return;
    }

    public function violations()
    {
        $sql = <<<SQL
        SELECT
        v.violation_id,
        vc.name as category

        FROM user u
join violation v on v.user_id = u.user_id
join violation_category vc on vc.violation_category_id = v.violation_category_id
WHERE u.user_id =
SQL;

        $sql .= $this->getKey();
        $sql .= " ORDER BY v.violation_id";
        $stmt = DB::dbh()->query($sql);

        $violations = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $return = array();

        $labels = array(
            "violation_id" => "#",
            "created" => "Datum",
            "ending" => "Zastara",
            "category" => "Kategorija",
        );

//        dd($violations);

        if (count($violations)) {
            foreach ($violations as $result) {
                $row = array();
                foreach ($result as $k => $value) {
                    if (in_array($k, array("created", "ending")) && $value != "NIKADA") {
                        $value = date("d.m.y H:i", strtotime($value));
                    }
                    $row[] = array("label" => $labels[$k], "value" => $value);
                }
                $return[$result["violation_id"]] = $row;
            }
        }
        return $return;
    }

    public function complaints()
    {
        $sql = <<<SQL
        SELECT v.violation_id, c.complaint_id, IF( c.complaint_status_id =1,  "NA ČEKANJU", IF( c.complaint_status_id =2,  "PRIHVACENA",  "ODBIJENA" ) ) AS
STATUS , vc.name AS category
FROM user u
JOIN violation v ON v.user_id = u.user_id
JOIN violation_category vc ON vc.violation_category_id = v.violation_category_id
JOIN complaint c ON c.violation_id = v.violation_id
WHERE u.user_id =
SQL;

        $sql .= $this->getKey();
        $sql .= " ORDER BY c.complaint_id";
        $stmt = DB::dbh()->query($sql);

        $complaints = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $return = array();

        $labels = array(
            "complaint_id" => "#",
            "violation_id" => "Prekršaj",
            "STATUS" => "Status",
            "category" => "Kategorija",
        );

        if (count($complaints)) {
            foreach ($complaints as $result) {
                $row = array();
                foreach ($result as $k => $value) {
                    if (in_array($k, array("created", "ending")) && $value != "NIKADA") {
                        $value = date("d.m.y H:i", strtotime($value));
                    }
                    $row[] = array("label" => $labels[$k], "value" => $value);
                }
                $return[$result["complaint_id"]] = $row;
            }
        }
        return $return;
    }

    public function complaintsStats()
    {
        $sql = <<<SQL
        SELECT c.complaint_status_id, v.user_id, count(*) as count from complaint c join violation v on v.violation_id = c.violation_id group by 1,2 having v.user_id =
SQL;

        $sql .= $this->getKey();
        $stmt = DB::dbh()->query($sql);

        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

}