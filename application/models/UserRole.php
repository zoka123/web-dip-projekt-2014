<?php

class UserRole extends Model
{
    public static $attributes = array();
    protected static $table = "user_role";

    protected static $primaryKey = "user_role_id";

    public static $editable = array(
        "name",
    );
} 