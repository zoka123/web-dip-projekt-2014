<?php

class File extends Model
{
    public static $imageTypes = array("jpg", "png", "gif", "jpeg");
    public static $documentTypes = array("pdf");

    public static $attributes = array();
    protected static $table = "file";
    protected static $primaryKey = "file_id";

    const FILE_TYPE_EVIDENCE = 1;
    const FILE_TYPE_COMPLAINT = 2;

    public static function getFileType($typeId){
        $file_types = array(
            $FILE_TYPE_EVIDENCE = "Dokaz/Izjava",
            $FILE_TYPE_COMPLAINT = "Žalba",
        );

        return($file_types[$typeId]);
    }

    public static $editable = array(
        "extension",
        "filename",
        "type",
    );

}