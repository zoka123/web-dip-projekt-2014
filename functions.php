<?php

/*
 * Dump and Die function
 * Exit script and dump provided var
 * */
function dd($var, $exit = true)
{
    echo '<pre>' . print_r($var, true) . '</pre>';
    if ($exit === true) {
        exit();
    }
}

function zf_error_handler($errno, $errstr, $errfile, $errline)
{
    debug(array($errno, $errstr, $errfile, $errline));
    return View::render("error.twig.html", array("error" => $errstr));
}

function zf_exception_handler($exception)
{
    debug($exception);
    return View::render("error.twig.html", array("error" => $exception->getMessage()));
}

function debug($msg)
{
    $mode = Config::get("app_mode");
    if (Config::isDevelopment()) {
        dd($msg, false);
    }
}

function datetime($time = null)
{
    if ($time == null) {
        $time = VirtualTime::vtime();
    } elseif (!is_int($time)) {
        $time = strtotime($time);
    }

    return date('Y-m-d H:i:s', $time);
}